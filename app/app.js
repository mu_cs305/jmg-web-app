﻿//declare our angularJS app on the global scope:
app = angular.module('app', ['ngAnimate', 'ui.router', 'LocalStorageModule', 'datatables'])
    .constant("CONFIG", {
        version: 1.05,

        // you'll want to change this string to your localhost:<port #>/api address for development
        //API: 'http://localhost:51962/',
        API: 'https://scratch-red.apphb.com/',
        DEBUG: false,

        CLOUD_NAME : 'duyfjnu2h',
        BUG_UPLOAD_PRESET: 'rjws4ok2',
        PLANT_UPLOAD_PRESET: 'yvot0j2v',
        CLOUDINARY_API: 'https://api.cloudinary.com/v1_1/duyfjnu2h/', //string at the end of this address has to match cloud name,

        CLOUDINARY_API_SECRET: '5zk06U5PnCN9MF2R8vDEU97NXDQ', //Probably shouldn't be storing our API secret here ¯\_(ツ)_/¯
        CLOUDINARY_API_KEY: '174459455699835',
        CLOUDINARY_AUTH_API: 'https://174459455699835:5zk06U5PnCN9MF2R8vDEU97NXDQ@api.cloudinary.com/v1_1/duyfjnu2h/'

    })

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    })

    .run(function ($state, $rootScope, routeChangeInterceptor) {
        //setup route change interception:
        routeChangeInterceptor.listen();

        $rootScope.showNav = true;
        $rootScope.toggleNav = function(setting){
            $rootScope.showNav = setting;
        };

        //we can do any setup here if we need to.
        $state.go('login');
    });
