app.directive("navTitle",['$log','$rootScope', function($log, $rootScope){
    return {
        restrict: 'A',
        link: function(scope, elem, attr){
            elem.hide();
            var text = elem.text();
            $rootScope.navTitle = text;
        }
    };

}]);