//Directive for a simple info message on a light-colored card
app.directive("infoBar", [function(){
    return {
        restrict: 'E',
        scope: {
            message: "@",
            acknowledge: "="
        },
        templateUrl: 'app/js/utility/info-bar/info-bar.html'
    }
}]);