app.directive("spinner", [function(){
    return {
        restrict: "E",
        template: '<svg class="spinner" width="100px" height="100px" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">'
            + '<circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="50" cy="50" r="44"></circle>'
            + '</svg'
    }
}]);