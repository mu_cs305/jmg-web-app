//Directive for a simple error
app.directive("errorBar", [function(){
    return {
        restrict: 'E',
        scope: {
            message: "@",
            acknowledge: "="
        },
        templateUrl: 'app/js/utility/error-bar/error-bar.html'
    }
}]);