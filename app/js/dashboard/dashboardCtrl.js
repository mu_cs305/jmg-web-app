﻿app.controller('dashboardController', ['$scope', 'userService',
    function ($scope, userService) {
        $scope.isAdmin = userService.isAdmin();
    }]);