﻿//TODO: password reset
app.controller('loginController', ['$log', '$scope', 'authService', 'navService', '$stateParams',
    function ($log, $scope, authService, navService, $stateParams) {

        $scope.loginData = {
            userName: "",
            password: ""
        };
        $scope.loading = false;

        $scope.errorMessage = null;
        $scope.acknowledge = function(){
            $scope.errorMessage = null;
        };

        $scope.specialMessage = $stateParams.specialMessage;

        $scope.login = function () {

            $scope.loading = true;
            $scope.errorMessage = "";

            authService.login($scope.loginData)
                .then(function (response) {
                    navService.goToDashboard();
                })
                .catch(function(){
                    //Okay, first one failed, let's try again.
                    //This is a dirty, band-aid fix, and I'm not proud of it.
                    return authService.login($scope.loginData)
                        .then(function (response) {
                            navService.goToDashboard();
                        })
                        .catch(function (err) {
                            if(err && err.error_description){
                                $scope.errorMessage = err.error_description;
                            }
                            else{
                                $log.error("Error logging in: \n" + JSON.stringify(err));
                                $scope.errorMessage = "Could not reach server."
                            }
                        });
                })
                .finally(function () {
                    $scope.loading = false;
                });
        };

        $scope.adminLogin = function(){
            $scope.loginData.userName = "superuser@super.com";
            $scope.loginData.password = "SuperPass";
            $scope.login();
        }

    }]);