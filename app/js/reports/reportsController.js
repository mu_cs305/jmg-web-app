app.controller("reportsController", ['$scope', '$log', 'studentService', 'testSessionService', 'reportService', 'navService',
function($scope, $log, studentService, testSessionService, reportService, navService){
    $scope.loading = false;
    $scope.error = false;

    $scope.formError = null;

    $scope.chapters = [
        { num: 1, selected: false},
        { num: 2, selected: false},
        { num: 3, selected: false},
        { num: 4, selected: false},
        { num: 5, selected: false},
        { num: 6, selected: false}
    ];

    $scope.load = function(){
        $scope.error = false;
        $scope.loading = true;
        studentService.getStudents()
            .then(function(students){
                $scope.students = students;
            })
            .catch(function(error){
                $scope.error = true;
                Materialize.toast('Failed to load Students!', 6000);
            })
            .finally(function(){
                $scope.loading = false;
            });
        testSessionService.getAll()
            .then(function(sessions){
                $scope.sessions = sessions;
                $scope.locations = [];
                _.forEach($scope.sessions, function(session){
                    $scope.locations.push(session.assessment.location);
                });
                $scope.locations = _.uniqBy($scope.locations, function(loc){
                    return loc.locationID;
                });
            })
            .catch(function(error){
                $scope.error = true;
                Materialize.toast('Failed to load Test Sessions!', 6000);
            })
            .finally(function(){
                $scope.loading = false;
            });
    };
    $scope.load();

    $scope.toggleSelected = function(item){
        item.selected = !item.selected;
    };

    // Location manipulation
    $scope.selectAllLocations = function(){
        _.forEach($scope.locations, function(loc){
            loc.selected = true;
        });
    };
    $scope.selectNoLocations = function(){
        _.forEach($scope.locations, function(loc){
            loc.selected = false;
        });
    };

    // Student manipulation
    $scope.selectAllStudents = function(){
        _.forEach($scope.students, function(student){
            student.selected = true;
        });
    };
    $scope.selectNoStudents = function(){
        _.forEach($scope.students, function(student){
            student.selected = false;
        });
    };

    //Chapter manipulations
    $scope.selectAllChapters = function(){
        _.forEach($scope.chapters, function(chapter){
            chapter.selected = true;
        })
    };
    $scope.selectNoChapters = function(){
        _.forEach($scope.chapters, function(chapter){
            chapter.selected = false;
        })
    };

    $scope.acknowledge = function(){
        $scope.error = false;
    };

    $scope.acknowledgeFormError = function(){
        $scope.formError = null;
    };


    $scope.generateReport = function(){
        $scope.formError = null;
        var selectedLocations = _.filter($scope.locations, function(loc){
            return loc.selected;
        });
        var selectedStudents = _.filter($scope.students, function(student){
            return student.selected;
        });
        var selectedChapters = _.filter($scope.chapters, function(chapter){
            return chapter.selected;
        });

        if(!selectedStudents.length){
            $scope.formError = "Please select at least one student."
        }
        else if(!selectedChapters.length){
            $scope.formError = "Please select at least one chapter."
        }
        else if(!selectedLocations.length){
            $scope.formError = "Please select at least one locations."
        }
        else{
            $scope.loading = true;
            var startDate = $('#datepicker1').val();
            var endDate = $('#datepicker2').val();
            reportService.generateReport(selectedLocations ,selectedStudents, selectedChapters, startDate, endDate)
                .then(function(report){
                    navService.goToViewReport(report);
                })
                .finally(function(){
                    $scope.loading = false;
                });
        }
    };

    $scope.downloadCSV = function(){
        $scope.formError = null;
        var selectedLocations = _.filter($scope.locations, function(loc){
            return loc.selected;
        });
        var selectedStudents = _.filter($scope.students, function(student){
            return student.selected;
        });
        var selectedChapters = _.filter($scope.chapters, function(chapter){
            return chapter.selected;
        });

        if(!selectedStudents.length){
            $scope.formError = "Please select at least one student."
        }
        else if(!selectedChapters.length){
            $scope.formError = "Please select at least one chapter."
        }
        else if(!selectedLocations.length){
            $scope.formError = "Please select at least one locations."
        }
        else{
            $scope.loading = true;
            var startDate = $('#datepicker1').val();
            var endDate = $('#datepicker2').val();
            reportService.downloadCSV(selectedLocations ,selectedStudents, selectedChapters, startDate, endDate)
                .then(function(report){
                    Materialize.toast("Downloaded!", 2000);
                }, function(error){
                    Materialize.toast("Error!", 5000);
                })
                .finally(function(){
                    $scope.loading = false;
                });
        }
    }
}]);