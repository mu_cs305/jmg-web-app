/*
 Report Object will look like this:
 {
     results: [
         {
            studentID: 5
             totalPctCorrect: 0.90,
             correctCount: 9,
             incorrectCount: 1,
             bestChapter: 3,
             worstChapter: 4,
             averageTime: {
                minutes: 8,
                seconds: 17
             },
             chapterResults: {
                 1: {
                     pctCorrect: 0.70,
                     correctCount: 5,
                     incorrectCount: 1,
                     answers: [ <list of answer objects>],
                 },
                 2: {
                 ...
                 }
            },
         },
         {
         studentID: 2
         ...
         }
     ],
     errors:[]  //array of error messages
 }

 This is a convenient format for our web app to display results.  For the CSV export, we'll probably just
 want to do an answer dump so Dr. M can do whatever with it.
 */

app.factory("reportService", ['$log', '$q', 'studentService', function($log, $q, studentService){

    var service = {
        currentReport: null
    };

    //generates the report object specified above.  If start and end date aren't specified, all test sessions are considered
    service.generateReport = function(locations, students, chapters, startDt, endDt){
        var startDate = new Date(0); //1970
        var endDate = new Date(); //this very moment
        if(startDt){
            startDate = new Date(startDt);
        }
        if(endDt){
            endDate = new Date(endDt);
        }
        //Free ourselves from the restrictions of efficient processing by just deferring the whole thing
        var deferred = $q.defer();
        service.currentReport = {
            results: [],
            errors: []
        };

        //Defers call until the current function stack has cleared.
        _.defer(function(){
            var report = service.currentReport;
            //For each student, build chapter results and gather other stats
            _.forEach(students, function(student) {
                var studResultObj = {};

                studResultObj.studentID = student.studentID;
                studResultObj.pctCorrect = 0;
                studResultObj.correctCount = 0;
                studResultObj.incorrectCount = 0;
                studResultObj.bestChapter = null;
                studResultObj.worstChapter = null;
                studResultObj.chapterResults = {};
                studResultObj.averageTime = -1;
                //Check for error: student has no test sessions:
                if(!student.testSessions.length){
                    var er = service.buildError(student, "Student " + student.studentID + " has no test sessions within the date range!");
                    service.currentReport.errors.push(er);
                }
                else{
                    //Extract all answers and get average time
                    var timeSum = 0;
                    var answers = [];
                    _.forEach(student.testSessions, function(session){
                        var date = new Date(session.assessmentDate);
                        var location = session.assessment.location;
                        var includeLocation = _.find(locations, function(loc){
                            return loc.locationID == location.locationID;
                        });
                        if(includeLocation && dateCompare(date, startDate)>=0 && dateCompare(endDate, date) >= 0){
                            var sessionAnswers = session.answers;
                            answers = answers.concat(sessionAnswers);

                            timeSum += ( new Date(session.completionTime) - new Date(session.startTime));
                        }
                    });
                    if(student.testSessions.length){

                        var avgInMs = timeSum / student.testSessions.length;
                        var minutesDouble = avgInMs/60000;
                        var minutes = Math.floor(minutesDouble);
                        var remainder = minutesDouble % 1;
                        var seconds = Math.floor(remainder*60);
                        studResultObj.averageTime = {
                            minutes: minutes,
                            seconds: seconds
                        };
                    }
                    if(answers.length){
                        studResultObj.chapterResults = service.buildChapterResults(chapters, answers);

                        //Determine other stats defined above
                        _.forEach(studResultObj.chapterResults, function(chapterResult){
                            studResultObj.correctCount += chapterResult.correctCount;
                            studResultObj.incorrectCount += chapterResult.incorrectCount;
                            var resultTotalQuestions = chapterResult.incorrectCount + chapterResult.correctCount;
                            if(studResultObj.bestChapter){
                                var totalQuestions = studResultObj.worstChapter.incorrectCount + studResultObj.worstChapter.correctCount;
                                if((totalQuestions==0) ||
                                    (resultTotalQuestions!=0 && chapterResult.pctCorrect > studResultObj.bestChapter.pctCorrect)){
                                    studResultObj.bestChapter = chapterResult;
                                }
                            }
                            else{
                                studResultObj.bestChapter = chapterResult;
                            }
                            if(studResultObj.worstChapter){
                                var totalQuestions = studResultObj.worstChapter.incorrectCount + studResultObj.worstChapter.correctCount;
                                if((totalQuestions==0) ||
                                    (resultTotalQuestions!=0 && chapterResult.pctCorrect < studResultObj.bestChapter.pctCorrect)){
                                    studResultObj.worstChapter = chapterResult;
                                }
                            }
                            else{
                                studResultObj.worstChapter = chapterResult;
                            }
                        });
                        //now that we've determined the number of the best and worst chapter, reduce them to just the chapter number
                        studResultObj.bestChapter = studResultObj.bestChapter.chapter;
                        studResultObj.worstChapter = studResultObj.worstChapter.chapter;
                        var totalQuestions = studResultObj.correctCount + studResultObj.incorrectCount;
                        if(totalQuestions!=0){
                            studResultObj.pctCorrect = Math.round((studResultObj.correctCount/totalQuestions)*100)/100;
                        }
                        else{
                            var er = service.buildError(student, "Student " + student.studentID + " has no answers for the selected chapters!");
                            service.currentReport.errors.push(er);
                        }

                    } //end if
                    else{
                        //in the case that we don't have answers
                        var er = service.buildError(student, "Student " + student.studentID + " has no test records within the date range!");
                        service.currentReport.errors.push(er);
                    }
                }
                //insert the student into the results
                report.results.push(studResultObj);
            });

            deferred.resolve(report);
        });

        return deferred.promise;
    };

    service.downloadCSV = function(locations, students, chapters, startDt, endDt){
        var deferred = $q.defer();

        service.generateReport(locations, students, chapters, startDt, endDt)
            .then(function(report){
                //The top row labels the columns
                var csvString = "student_id, question, chapter, ordering, answer, is_correct, choices, question_time\n";
                _.forEach(report.results, function(student){
                    var allAnswers = [];
                    _.forEach(student.chapterResults, function(result){
                        allAnswers = allAnswers.concat(result.answers);
                    });
                    _.forEach(allAnswers, function(answer){
                        csvString += student.studentID + ",";
                        csvString += answer.question + ",";
                        csvString += answer.chapter + ",";
                        csvString += answer.answerID + ",";
                        csvString += answer.studentChoice + ",";
                        //detects if answer was correct
                        csvString += (service.isChoiceCorrect(answer.correctChoice, answer.studentChoice)) + ",";
                        csvString += answer.choices.replace(/,/g, "") + ",";
                        csvString += answer.timestamp;
                        csvString += "\n";
                    });
                });

                var filename = "report.csv";
                var blob = new Blob([csvString], { type: 'text/csv;charset=utf-8;' });
                if (navigator.msSaveBlob) { // IE 10+
                    navigator.msSaveBlob(blob, filename);
                } else {
                    var link = document.createElement("a");
                    if (link.download !== undefined) { // feature detection
                        // Browsers that support HTML5 download attribute
                        var url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", filename);
                        link.style.visibility = 'hidden';
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    } else{
                        //Fallback for browser's that don't support for the "download" attr
                        var csvURI = encodeURI(csvString);
                        window.open(csvURI);
                    }
                }
                deferred.resolve();
            }, function(error){
                deferred.reject();
            });

        return deferred.promise;
    };

    service.buildChapterResults = function(chapters, answers){
        var result = {};
        _.forEach(chapters, function(chapter){
            result[chapter.num] = service.buildChapterResult(chapter.num, answers);
        });

        return result;
    };

    //This isn't terribly efficient, since we go through the entire array 6 times.  If it turns out to be bad, we can optimize
    service.buildChapterResult = function(chapter, answers){
        var result = {
            chapter: chapter,
            pctCorrect: 0,
            correctCount: 0,
            incorrectCount: 0,
            answers: []
        };
        _.forEach(answers, function(answer){
            if(answer.chapter == chapter){
                result.answers.push(answer);
                if(service.isChoiceCorrect(answer.correctChoice, answer.studentChoice)){
                    result.correctCount++;
                }
                else{
                    result.incorrectCount++;
                }
            }
        });
        var totalQuestions = result.correctCount + result.incorrectCount;
        if(totalQuestions>0){
            result.pctCorrect = Math.round((result.correctCount/totalQuestions)*100)/100;
        }

        return result;
    };

    service.buildError = function(student, errorMessage){
        return {
            studentID: student.studentID,
            message: errorMessage
        };
    };

    service.isChoiceCorrect = function(correctChoice, studentChoice){
        var choices = correctChoice.split(",");
        var found = false;
        for(var i = 0; i < choices.length; i++){
            if(choices[i].trim().toLowerCase() == studentChoice.toLowerCase()){
                found = true;
            }
        }
        return found;
    };

    return service;

}]);

//Compares dates using the day only
//returns positive if date1 is greater than (later than) date2
//returns negative if date2 is greater than date1
//returns zero for the same day
function dateCompare(date1, date2){
    //Different years
    if(date1.getFullYear() > date2.getFullYear()){
        return 1;
    }
    else if(date2.getFullYear() > date1.getFullYear()){
        return -1
    }
    //else same year and function will continue
    if(date1.getMonth() > date2.getMonth()){
        return 1;
    }
    else if(date2.getMonth() > date1.getMonth()){
        return -1;
    }
    //else same month and function will continue
    if(date1.getDate() > date2.getDate()){
        return 1;
    }
    else if(date2.getDate() > date1.getDate()){
        return -1;
    }
    return 0; // same day
}