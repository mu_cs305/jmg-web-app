app.controller("studentDetailReportsController", ['$scope', '$log', '$stateParams', 'navService',
function($scope, $log, $stateParams, navService){
    $scope.result = $stateParams.result; //Result for the clicked student
    $scope.report = $stateParams.report; //Contains a reference to the whole report, in case we want to go back
    $scope.studentID = $stateParams.studentID;
    if(!$scope.result){
        navService.goToReports();
    }

    $scope.goBack = function(){
        navService.goToViewReport($scope.report);
    };

}]);