app.directive('selectInit', ['$timeout', function($timeout){
    return{
        restrict:'A',
        link:function(scope,elem,attrs){
            $timeout(function(){
                elem.material_select();
            },0)
        }
    }
}]);