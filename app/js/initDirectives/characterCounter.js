app.directive('charCounterInit', ['$timeout', function($timeout){
    return{
        restrict:'A',
        link:function(scope,elem,attrs){
            $timeout(function(){
                elem.characterCounter();
            },0)
        }
    }
}]);