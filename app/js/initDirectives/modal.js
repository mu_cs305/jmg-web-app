app.directive('modalInit', ['$timeout', function($timeout){
    return{
        restrict:'A',
        link:function(scope,elem,attrs){
            $timeout(function(){
                elem.leanModal();
            },0)
        }
    }
}]);