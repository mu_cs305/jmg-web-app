app.controller("plantGalleryController", ['$scope', '$q', '$http', 'imageService', 'plantService', 'CONFIG',
    function($scope, $q, $http, imageService, plantService, CONFIG){

        var init = function() {
            $scope.loading = true;
            plantService.getPlants()
                .then(function (plants) {
                    $scope.plants = plants;
                }).finally(function () {
                    $scope.loading = false;
                });
        };
        init();

        $scope.uploadImage = function(selectedPlant){
            cloudinary.openUploadWidget({ cloud_name: CONFIG.CLOUD_NAME, upload_preset: CONFIG.BUG_UPLOAD_PRESET},
                function(error, result) {
                    if (!error){
                        $scope.createNewPlantImage(selectedPlant, result);
                    }
                    else{
                        console.log("No images uploaded");
                    }
                }
            );
        };

        // Create 1:N new bug images
        $scope.createNewPlantImage = function(selectedPlant, upload_result) {
            $scope.loading = true;
            var promises = [];  //Array of promises.  We'll wait for all of these to be resolved before we say loading is done.
            // var createNewBugImage = function(selectedBug, upload_result) {
            for (var i = 0; i < upload_result.length; i++) {
                var plantImage = upload_result[i];
                var image = {};
                image.imageName = plantImage.public_id; //Filename will be the public_id of the resource
                image.imageHeight = plantImage.height;
                image.imageWidth = plantImage.width;
                image.imageUri = plantImage.url;
                image.fileType = plantImage.format;
                image.plantMatchInd = false;
                image.fruitVeggieInd = false;

                //This API call will both create the new plantImage and add it to the plant.
                // It's easier to do both operations server-side than try to do both here
                var promise = imageService.createPlantImage(image, selectedPlant.plantId)
                    .then(function(responseImage){
                        selectedPlant.images.push(responseImage);
                    }, function(error){
                        $scope.error = "Uh oh!  We had an error uploading one or more of your images.";
                    });

                promises.push(promise);
            }

            //$q.all returns a promise that resolves when all the promises are done.
            $q.all(promises).finally(function(){
                $scope.selected = selectedPlant;
                $scope.loading = false;
            });
        };

        //Deletes the image
        $scope.deleteImage = function(image, selectedPlant){
            $scope.loading = true;

            //Deletes image from cloudinary
            var deleteURL = CONFIG.CLOUDINARY_AUTH_API + 'resources/image/upload?public_ids=' + encodeURI(image.imageName);
            console.log("Attempting DELETE to: " + deleteURL);
            var promise1 = $http({
                method: 'DELETE',
                url: deleteURL,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded' //This isn't our actual content-type but it avoids a preflight
                }
            });
            promise1.then(function(response){
                console.log("Success deleting from cloudinary");
                console.log(JSON.stringify(response));
            }, function(error){
                console.log("Error deleting from cloudinary!!");
                console.log(JSON.stringify(error));
            });

            //delete image from our db
            var promise2 = imageService.deleteImage(image.imageID);

            //We really only have to make sure the image is gone from our DB.
            //That's why we only wait for promise 2 to finish before reporting success/failure.
            // It should also be deleted from cloudinary but it's a "bonus"
            promise2.then(function(response){
                $scope.selected = selectedPlant;
                Materialize.toast("Deleted!", 2000);
                //Remove image from view on success
                var index = $scope.selected.images.indexOf(image);
                $scope.selected.images.splice(index, 1);
            }, function(error){
                $scope.error = "Uh oh!  Something went wrong deleting that image";
            }).finally(function(){
                $scope.loading = false;
            });

        };

        $scope.useForFruitVeggie = function(image){
            $scope.loading = true;
            image.fruitVeggieInd = true;
            imageService.updatePlantImage($scope.selectedObjects.selected.plantId, image.imageID, image)
                .then(function(response){
                    //on success, use this image and disable others (like we did server-side)
                    _.forEach($scope.selectedObjects.selected.images, function(img){
                        img.fruitVeggieInd = false;
                    });
                    image.fruitVeggieInd = true;
                }, function(error){
                    //on failure, notify user and undo action
                    Materialize.toast("Failed!", 2000);
                    image.fruitVeggieInd = false;
                })
                .finally(function(){
                    $scope.loading = false;
                });
        };

        $scope.useForPlantMatch = function(image){
            $scope.loading = true;
            image.plantMatchInd = true;
            imageService.updatePlantImage($scope.selectedObjects.selected.plantId, image.imageID, image)
                .then(function(response){
                    //on success, use this image and disable others (like we did server-side)
                    _.forEach($scope.selectedObjects.selected.images, function(img){
                        img.plantMatchInd = false;
                    });
                    image.plantMatchInd = true;
                }, function(error){
                    //on failure, notify user and undo action
                    Materialize.toast("Failed!", 2000);
                    image.plantMatchInd = false;
                })
                .finally(function(){
                    $scope.loading = false;
                });
        };
    }
]);
