app.controller("galleryController", ['$scope', '$q', '$http', 'imageService', 'bugService', 'CONFIG',
    function($scope, $q, $http, imageService, bugService, CONFIG){
        $scope.status = {
            creating: false
        };
        $scope.loading = false;

        // uploadImage passes a copy of the bug and a successful upload result to createNewImage,
        // which prepares the new image for a PUT call on the selected bug.
        $scope.uploadImage = function(selectedBug){
            cloudinary.openUploadWidget({ cloud_name: CONFIG.CLOUD_NAME, upload_preset: CONFIG.BUG_UPLOAD_PRESET},
                function(error, result) {
                    if (!error){
                        //$scope.getImageInfo(selectedBug, result);
                        $scope.$apply(function() {
                            $scope.getImageInfo(selectedBug, result);
                        });

                    }
                    else{
                        console.log("No images uploaded");
                    }
                }
            );
        };

        $scope.getImageInfo = function(selectedBug, upload_result){
            $scope.status.creating = true;
            $scope.imageInfo = {
                isSheet: false,
                horizontalFrames: 0,
                verticalFrames: 0
            };
            $scope.continue = function(){
                $scope.status.creating = false;
                $scope.createNewBugImage(selectedBug, upload_result, $scope.imageInfo);
            };
        };

        // Create 1:N new bug images
        $scope.createNewBugImage = function(selectedBug, upload_result) {
            var promises = [];  //Array of promises.  We'll wait for all of these to be resolved before we say loading is done.
            // var createNewBugImage = function(selectedBug, upload_result) {
            for (var i = 0; i < upload_result.length; i++) {
                var bugImage = upload_result[i];
                var image = {};
                image.isSheet =  $scope.imageInfo.isSheet;
                image.horizontalFrames =  $scope.imageInfo.horizontalFrames;
                image.verticalFrames =  $scope.imageInfo.verticalFrames;
                image.imageName = bugImage.public_id; //Filename will be the public_id of the resource so we can reference it for deletion and stuff later
                image.imageHeight = bugImage.height;
                image.imageWidth = bugImage.width;
                image.imageUri = bugImage.url;
                image.fileType = bugImage.format;

                //This API call will both create the new bugImage and add it to the bug.
                // It's easier to do both operations server-side than try to do both here
                var promise = imageService.createBugImage(image, selectedBug.bugID)
                    .then(function (responseImage) {
                        selectedBug.images.push(responseImage);
                    }, function (error) {
                        $scope.error = "Uh oh!  We had an error uploading one or more of your images.";
                    });

                promises.push(promise);
            }

            //$q.all returns a promise that resolves when all the promises are done.
            $q.all(promises).finally(function () {
                $scope.selected = selectedBug;
                $scope.loading = false;
            });
        };

        //Deletes the image
        $scope.deleteImage = function(image, selectedBug){
            $scope.loading = true;

            //Deletes image from cloudinary
            var deleteURL = CONFIG.CLOUDINARY_AUTH_API + 'resources/image/upload?public_ids=' + encodeURI(image.imageName);
            console.log("Attempting DELETE to: " + deleteURL);
            var promise1 = $http({
                method: 'DELETE',
                url: deleteURL,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded' //This isn't our actual content-type but it avoids a preflight
                }
            });
            promise1.then(function(response){
                console.log("Success deleting from cloudinary");
                console.log(JSON.stringify(response));
            }, function(error){
                console.log("Error deleting from cloudinary!!");
                console.log(JSON.stringify(error));
            });

            //delete image from our db
            var promise2 = imageService.deleteImage(image.imageID);

            //We really only have to make sure the image is gone from our DB.
            //That's why we only wait for promise 2 to finish before reporting success/failure.
            // It should also be deleted from cloudinary but it's a "bonus"
            promise2.then(function(response){
                $scope.selected = selectedBug;
                Materialize.toast("Deleted!", 2000);
                //Remove image from view on success
                var index = $scope.selected.images.indexOf(image);
                $scope.selected.images.splice(index, 1);
            }, function(error){
                $scope.error = "Uh oh!  Something went wrong deleting that image";
            }).finally(function(){
                $scope.loading = false;
            });

        };

        $scope.useForBugMatch = function(image){
            $scope.loading = true;
            image.bugMatchInd = true;
            imageService.updateBugImage($scope.selectedObjects.selected.bugID, image.imageID, image)
                .then(function(response){
                    //on success, use this image and disable others (like we did server-side)
                    _.forEach($scope.selectedObjects.selected.images, function(img){
                        img.bugMatchInd = false;
                    });
                    image.bugMatchInd = true;
                }, function(error){
                    //on failure, notify user and undo action
                    Materialize.toast("Failed!", 2000);
                    image.bugMatchInd = false;
                })
                .finally(function(){
                    $scope.loading = false;
                });

        };

        $scope.useForBugSort = function(image){
            $scope.loading = true;
            image.bugSortInd = true;
            imageService.updateBugImage($scope.selectedObjects.selected.bugID, image.imageID, image)
                .then(function(response){
                    //on success, use this image and disable others (like we did server-side)
                    _.forEach($scope.selectedObjects.selected.images, function(img){
                        img.bugSortInd = false;
                    });
                    image.bugSortInd = true;
                }, function(error){
                    //on failure, notify user and undo action
                    Materialize.toast("Failed!", 2000);
                    image.bugSortInd = false;
                })
                .finally(function(){
                    $scope.loading = false;
                });

        }
    }
]);
