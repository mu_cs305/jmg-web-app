app.controller("menuController", ['$scope', '$log', 'userService', function($scope, $log, userService){
    $scope.$watch(function(){
        return userService.getUser();
    }, function(){
        $scope.isAdmin = userService.getUser().isAdmin;
    }, true)

}]);

