app.factory("gameService", ['dimensionsService', function(dimensionsService){
    var service = {};

    // We "expect" a screen of this size
    // All assets will be scaled to this aspect ratio
    service.expectedWidth = 800;
    service.expectedHeight = 600;
    service.width = dimensionsService.getWidth();
    service.navBarHeight = $("#jmg-navbar").height(); //It's bad practice to use Jquery in an angular app, but sometimes we have to compromise
    service.height = dimensionsService.getHeight();
    service.widthRatio = service.width/service.expectedWidth;
    service.heightRatio = service.height/service.expectedHeight;

    /*
    Creates a new game instance
    By default, the game will fill up the available viewport
    Params:
        - preloadFn: function called during the preload stage of the Phaser cycle.  See Phaser docs
        - createFn: function called during the create stage of the Phaser cycle.  See Phaser docs
        - updateFn: function called during the update stage (called every frame) of the Phaser cycle.  See Phaser docs
        - DOMContainerID: string: id of the dom element that will contain the game
    Returns: instance of Phaser.Game
     */
    service.startNewGame = function(preloadFn, createFn, updateFn, DOMContainerID){

        var game = new Phaser.Game(service.width, service.height, Phaser.AUTO, DOMContainerID, {
            preload: preloadFn,
            create: createFn,
            update: updateFn
        });

        return game;
    };

    /*
    This method loads an image in the specified game instance from the given path with the given name
    The name of the loaded image is returned.  The name can be used throughout phaser to reference the image
     */
    service.loadImage = function(game, imageName, path){
        game.load.image(imageName, path);
        return imageName;
    };

    /*
     This method loads all the specified spritesheets
     game is an instance of Phaser.Game
     sheets is an array of objects with the following pattern:
     [
         {
             name: String,
             path: String,
             width: Number, (the width of each frame on the sheet)
             height: Number (the height of each frame on the sheet)
         },
        ...
     ]
     Returns a list of spritesheet names that were successfully loaded
     */
    service.loadSpriteSheets = function(game, sheets){
        var sheetNames = [];
        _.forEach(sheets, function(sheet){
            game.load.spritesheet(sheet.name, sheet.path,
                sheet.width,
                sheet.height);
            sheetNames.push(sheet.name);
        });
        return sheetNames;
    };

    /*
     This method loads a single spritesheet
     game is an instance of Phaser.Game
     sheet is an object with the following pattern:
         {
             name: String,
             path: String,
             width: Number, (the width of each frame on the sheet)
             height: Number (the height of each frame on the sheet)
         }
     Returns the name of the spritesheet that was successfully loaded
     */
    service.loadSpriteSheet = function(game, sheet){
        game.load.spritesheet(sheet.name, sheet.path,
            sheet.width,
            sheet.height);
        return sheet.name;
    };

    //TODO: stub
    /*
    This method loads all the specified audio files
    game is an instance of Phaser.Game
    files is an array of objects with the following pattern:
    [
        {
            name: String,
            path: String
        }
    ]
     */
    service.loadAudioFiles = function(game, files){

    };

    /*
    This method adds a sprite at the indicated coordinates.  Scaling is done automatically.
    xCoord and yCoord are the coordinates where the sprite will be added.
    width and height are the width and height relative to the "expected" screen size of 800x600
    if width or height is null, then the image maintains its aspect ratio
    imageName is the name of the image as tracked by Phaser - corresponds to the imageName passed to loadImages()
    centerAnchor is an optional parameter;  If true, the sprite's anchor point will be set to its center instead of its top-left corner
    Should return the sprite object
     */
    service.addSprite = function(game, xCoord, yCoord, width, height, imageName, centerAnchor){
        var sprite = game.add.sprite(xCoord*service.widthRatio,
            yCoord *service.heightRatio,
            imageName);
        if(width && !height){
            var ratio = sprite.height/sprite.width;
            sprite.width = width*service.widthRatio;
            sprite.height = sprite.width*ratio;
        } else if(!width && height){
            var ratio = sprite.width/sprite.height;
            sprite.height = height*service.heightRatio;
            sprite.width = ratio*sprite.height;
        } else{
            sprite.width = width*service.widthRatio;
            sprite.height = height*service.heightRatio;
        }
        if(centerAnchor){
            sprite.anchor.setTo(0.5,0.5);
        }
        return sprite;
    };

    service.addSpriteUnscaled = function(game, xCoord, yCoord, width, height, imageName, centerAnchor){
        var sprite = game.add.sprite(xCoord,
            yCoord,
            imageName);
        if(width && !height){
            var ratio = sprite.height/sprite.width;
            sprite.width = width*service.widthRatio;
            sprite.height = sprite.width*ratio;
        } else if(!width && height){
            var ratio = sprite.width/sprite.height;
            sprite.height = height*service.heightRatio;
            sprite.width = ratio*sprite.height;
        } else{
            sprite.width = width*service.widthRatio;
            sprite.height = height*service.heightRatio;
        }
        if(centerAnchor){
            sprite.anchor.setTo(0.5,0.5);
        }
        return sprite;
    };

    /*
     This method adds text at the indicated coordinates.  Scaling is done automatically.
     xCoord and yCoord are the coordinates where the text will be added relative to the expected screen size
     fontSize is the font size relative to the expected height of 600
     imageName is the name of the image as tracked by Phaser - corresponds to the imageName passed to loadImages()
     fillColor is the hex color of the font color
     Should return the text object
     */
    service.addText = function(game, xCoord, yCoord, width, height,  string, fontSize, fillColor){
        var text = game.add.text(xCoord*service.widthRatio, yCoord*service.heightRatio, string,{ font: fontSize+'px Arial', fill: fillColor });
        if(width && !height){
            var ratio = text.height/text.width;
            text.width = width*service.widthRatio;
            text.height = text.width*ratio;
        } else if(!width && height){
            var ratio = text.width/text.height;
            text.height = height*service.heightRatio;
            text.width = ratio*text.height;
        } else{
            text.width = width*service.widthRatio;
            text.height = height*service.heightRatio;
        }
        text.anchor.set(0.5);
        return text;
    };

    /*
    This method generates a path through the given coords using Catmull-Rom interpolation
    coords is an array of objects with and x param and y param specifying x and y coordinates
    eg: {x: 5, y: 10}
    The x and y coords will be scaled
    This method returns an array of path points with the following form:
    [
        {
            x: Number,
            y: Number,
            angle: Number
        }
    ]
     */
    service.generatePath = function(game, xPoints, yPoints, count){
        if(xPoints.length != yPoints.length){

        } else{
            //Scale the points
            for(var i = 0; i < xPoints.length; i++){
                xPoints[i] = xPoints[i]*service.widthRatio;
                yPoints[i] = yPoints[i]*service.heightRatio;
            }
            var path = [];
            var x = 1/game.width; //interpolation step size
            if(count){
                x = 1/count;
            }
            for (i = 0; i <= 1; i += x) {
                //compute path x and y coords
                var px = game.math.catmullRomInterpolation(xPoints, i);
                var py = game.math.catmullRomInterpolation(yPoints, i);
                //compute angle relative to the x axis
                //This will exclude the angle for the first point
                if(path.length){
                    var pa =
                        game.math.angleBetweenPoints({x: path[path.length-1].x, y: path[path.length-1].y}, {x: px, y:py})
                        +Math.PI/2; //correctly factor of 90 degrees since phaser assumes sprites will be in landscape orientation
                }
                path.push({x: px, y: py, angle: pa});
            }

            //Compute the angle for the first point:
            path[0].angle = game.math.angleBetweenPoints({x: path[0].x, y: path[0].y}, {x: path[1].x, y: path[1].y});
            return path;
        }

    };

    /*
        Changes the position of the given sprite relative to the assumed window size of 800x600
        x : x-coordinate to which the sprite will be moved.  Relative to width of 800
        y : y-coordinate to which the sprite will be moved.  Relative to height of 600
        angle: angle in radians to rotate the sprite (CW)
     */
    service.changePosition = function(sprite, x, y, angle){
        sprite.x = x * service.widthRatio;
        sprite.y = y * service.heightRatio;
        sprite.rotation = angle;
    };

    //TODO: add any other necessary methods
    /*
    Off the top of my head, we'll probably want a method for defining paths for entities like sprites, and
    maybe a method to add buttons with handlers
     */

    return service;
}]);