
app.controller('fruitVeggieGameController', ['$scope', '$rootScope', 'currentTestService', 'gameService', 'pubSubService', 'voiceService', 'navService', 'settings',
    function($scope, $rootScope, currentTestService, gameService, pubSubService, voiceService, navService, settings){

        var game = gameService.startNewGame(preload, create, update, 'game-container');

        var phaser_dude = null;

        var cursors;

        var answers = [];

        //manually selected coordinates... all may or may not be used. Need to add more unless random placemnet function is created
        var assessmentPackets = JSON.parse(JSON.stringify(currentTestService.getCurrentAssessment().seedPackets));
        var assessmentsPlants = _.map(assessmentPackets, function(packet){
            return packet.plant;
        });
        var items = shuffle(assessmentsPlants);
        //items = _.take(items, 20);
        var coordinates = [];
        //Build random coordinate pairs
        var mapItems = [];

        var walls;
        var player;
        var menu;
        var fruitBasket;
        var vegetableBasket;
        var paused = false;
        var pickedFruit;
        var fruitLabel;
        var activeItemIndex;
        var guess;
        var FRUIT = 0;  var VEGETABLE = 1; //arbitrary constants to represent guess values.  Faster than string comparisons
        var guessCount = 0;
        var correctCount = 0;
        var correctAnswers;
        var itemsLeftText;
        var itemsLeft = items.length;

        var mapPlayer;

        var up = false;
        var down = false;
        var right = false;
        var left = false;

        function preload(){
            game.load.crossOrigin = 'anonymous';  //allows CORS requests to CDN
            game.load.image('bg','app/assets/images/davidBackdrop.png');
            game.load.spritesheet('phaser_dude','app/assets/images/phaser_dude.png',32,48);
            walls = game.load.image('walls','app/assets/images/walls.png');
            game.load.physics('walls', 'app/assets/walls.json');
            game.load.image('menu','app/assets/images/menu.png');
            game.load.image('fruits','app/assets/images/fruit.png');
            game.load.image('veggies','app/assets/images/veggie.png');
            game.load.image('map','app/assets/images/map.png');
            game.load.image('mapPlayer','app/assets/images/mapPlayer.png');
            game.load.image('mapItem','app/assets/images/mapItem.png');

            game.load.image('itemsLeftBackground','app/assets/images/itemsLeftBackground.png');

            exitButtonName = gameService.loadImage(game, 'exitBtn', 'app/assets/images/UI/exit.png');

            game.load.spritesheet('button-vertical', 'app/assets/images/button-vertical.png');
            game.load.spritesheet('button-horizontal', 'app/assets/images/button-horizontal.png');
            //new way to load fruits and veggies
            items.forEach(function(plant){
                game.load.image(plant.plantName,plant.images[0].imageUri);
            });

            voiceService.preloadRobot(game);
        }

        function create(){
            game.add.tileSprite(0, 0, 3840, 3840, 'bg');
            walls = game.add.sprite(1904, 1904, 'walls');
            player = game.add.sprite(1920, 1920, 'phaser_dude');
            player.scale.setTo(2, 2);

            voiceService.createRobot(game);

            game.world.setBounds(0, 0, 3840, 3840);

            game.physics.startSystem(Phaser.Physics.P2JS);
            game.physics.p2.enable(player);
            player.body.fixedRotation = true;
            game.physics.p2.setImpactEvents(true);
            game.physics.p2.enable(walls);
            walls.enableBody = true;
            player.enableBody = true;
            walls.body.static = true;
            walls.body.clearShapes();
            walls.body.loadPolygon('walls','walls');

            //define collision groups
            var itemGroup = game.physics.p2.createCollisionGroup();
            var playerGroup = game.physics.p2.createCollisionGroup();
            var wallGroup = game.physics.p2.createCollisionGroup();

            setCoordinates();



            player.animations.add('left', [0, 1, 2, 3], 10, true);
            player.animations.add('right', [5, 6, 7, 8], 10, true);

            //does all the work for everything in the 'items' array (all fruits and veggies)
            items.forEach(function(item,index){
                item.sprite = game.add.sprite(coordinates[index].x,coordinates[index].y,item.plantName);
                item.sprite.width = 50; item.sprite.height = 50;
                game.physics.p2.enable(item.sprite);
                game.physics.p2.enableBody(item.sprite);
                item.sprite.body.setCollisionGroup(itemGroup);
                item.sprite.body.collides(playerGroup);
                item.sprite.body.static = true;
            });

            var map = game.add.sprite(game.camera.x+game.camera.width-300, game.camera.y+game.camera.height - 300, 'map');
            map.fixedToCamera = true;

            items.forEach(function(item,index) {
                var mapItem = {
                    sprite: game.add.sprite(0, 0, 'mapItem'),
                    plantName: item.plantName
                };
                mapItems.push(mapItem);
                mapItem.sprite.fixedToCamera = true;
                mapItem.sprite.anchor.setTo(0.5,0.5);
                mapItem.sprite.cameraOffset.x = item.sprite.position.x/15.36 + (game.camera.width - 300);
                mapItem.sprite.cameraOffset.y = item.sprite.position.y/15.36 + (game.camera.height - 300);
            });

            player.body.collides(itemGroup);

            player.body.setCollisionGroup(playerGroup);
            walls.body.setCollisionGroup(wallGroup);
            player.body.collides(wallGroup);
            walls.body.collides(playerGroup);

            game.physics.p2.updateBoundsCollisionGroup();



            mapPlayer = game.add.sprite(game.camera.x+game.camera.width-300+ 137, game.camera.y+game.camera.height - 300 + 137, 'mapPlayer')
            mapPlayer.fixedToCamera = true;
            mapPlayer.anchor.setTo(0.5, 0.5);


            var ilbg = game.add.sprite(game.camera.x + 25, game.camera.y + 25, 'itemsLeftBackground');
            ilbg.fixedToCamera = true;

            itemsLeftText = game.add.text( game.camera.x + 44, game.camera.y + 37, "Items remaining: " + itemsLeft, { font: '24px Arial', fill: '#fff' });
            itemsLeftText.fixedToCamera = true;

            var cabg = game.add.sprite(game.camera.x + game.camera.width - 279, game.camera.y + 25, 'itemsLeftBackground');
            cabg.fixedToCamera = true;

            correctAnswers = game.add.text( game.camera.x + game.camera.width - 260, game.camera.y + 37, "Correct answers: " + correctCount, { font: '24px Arial', fill: '#fff' });
            correctAnswers.fixedToCamera = true;

            var buttonleft = game.add.button(game.camera.x, game.camera.y+game.camera.height - 128-64, 'button-horizontal', null, this, 0, 1, 0, 1);
            buttonleft.fixedToCamera = true;
            buttonleft.events.onInputDown.add(function(){left=true;});
            buttonleft.events.onInputUp.add(function(){left=false;});

            var buttonright = game.add.button(game.camera.x+128+64,game.camera.y+game.camera.height - 128-64 , 'button-horizontal', null, this, 0, 1, 0, 1);
            buttonright.fixedToCamera = true;
            buttonright.events.onInputDown.add(function(){right=true;});
            buttonright.events.onInputUp.add(function(){right=false;});

            var buttondown = game.add.button(game.camera.x + 128, game.camera.y+game.camera.height - 128, 'button-vertical', null, this, 0, 1, 0, 1);
            buttondown.fixedToCamera = true;
            buttondown.events.onInputDown.add(function(){down=true;});
            buttondown.events.onInputUp.add(function(){down=false;});

            var buttonup = game.add.button(game.camera.x + 128, game.camera.y+game.camera.height - 128-64-128, 'button-vertical', null, this, 0, 1, 0, 1);
            buttonup.fixedToCamera = true;
            buttonup.events.onInputDown.add(function(){up=true;});
            buttonup.events.onInputUp.add(function(){up=false;});

            //Show the exit/abort button
            var exit = gameService.addSprite(game, 750, 550, 25, null, exitButtonName);
            exit.fixedToCamera = true;
            exit.inputEnabled = true;
            exit.events.onInputDown.add(function(){
                if(prompt("Are you sure you wish to abort?  The student's progress will not be saved.\n" +
                        "Please enter the PIN to continue") == settings.pin){
                    //exit without saving answers
                    navService.goToGameLobby();
                }
            }, this);

            voiceService.speak('Welcome! Move around to collect all of the fruits and vegetables in the house and place them in the correct basket.', 0.9, 1);


            //happens when the player touches a fruit or vegetable
            player.body.onBeginContact.add(function (bodyA,bodyB,shapeA,shapeB,equation) {
                if(bodyA.sprite.key != 'walls' && !paused) {

                    //prevents player movement until choice is made
                    paused = true;

                    //finds the item in the 'items' array
                    items.forEach(function(item,index){
                        if(item.plantName == bodyA.sprite.key){
                            activeItemIndex = index;
                        }
                    });

                    //creates the menu
                    menu = game.add.sprite(game.camera.width / 2, game.camera.height / 2, 'menu');
                    menu.anchor.setTo(0.5, 0.5);
                    menu.fixedToCamera = true;

                    //adds the fruit basket
                    fruitBasket = game.add.sprite(game.camera.width/2 - 300, game.camera.height/2 + 100, 'fruits');
                    fruitBasket.anchor.setTo(0.5,0.5);
                    fruitBasket.fixedToCamera = true;
                    fruitBasket.inputEnabled = true;
                    fruitBasket.events.onInputUp.add(function(){
                        guess = FRUIT;
                        unpause();
                    });


                    //adds the vegetable basket
                    vegetableBasket = game.add.sprite(game.camera.width/2 + 300, game.camera.height/2 + 100, 'veggies');
                    vegetableBasket.anchor.setTo(0.5,0.5);
                    vegetableBasket.fixedToCamera = true;
                    vegetableBasket.inputEnabled = true;
                    vegetableBasket.events.onInputUp.add(function(){
                        guess = VEGETABLE;
                        unpause();
                    });


                    //display the fruit OR VEGGIE the player touched (confusing variable name, will change)
                    pickedFruit = game.add.sprite(game.camera.width/2,game.camera.height/2 - 90,items[activeItemIndex].plantName);
                    pickedFruit.aspectRatio = pickedFruit.width/pickedFruit.height;
                    pickedFruit.height = game.camera.height/5;
                    pickedFruit.width = pickedFruit.aspectRatio * pickedFruit.height;
                    pickedFruit.anchor.setTo(0.5, 0.5);
                    pickedFruit.fixedToCamera = true;

                    //shows the name of the item
                    fruitLabel = game.add.text( game.camera.width/2, game.camera.height/2 + 125, items[activeItemIndex].plantName.toUpperCase().replace(/ /g, '\n'), { font: '40px Arial', fill: '#fff' });
                    fruitLabel.anchor.set(0.5);
                    fruitLabel.fixedToCamera = true;

                }

            });



            cursors = game.input.keyboard.createCursorKeys();

            game.camera.follow(player);
        }

        function setCoordinates(){
            items.forEach(function(item){
                var sector = Math.random()*4 + 1;
                if(sector <= 2){
                    coordinates.push({
                        x: 150 + Math.random()*3540,
                        y: 200 + Math.random()*900
                    })
                } else if (sector <= 3) {
                    coordinates.push({
                        x: 150 + Math.random()*1350,
                        y: 1280 + Math.random()*(2650-1280)
                    })
                } else if (sector <= 4) {
                    coordinates.push({
                        x: 2280 + Math.random()*(3700-2280),
                        y: 1280 + Math.random()*(2650-1280)
                    })
                } else if (sector <= 5) {
                    coordinates.push({
                        x: 150 + Math.random()*3540,
                        y: 2790 + Math.random()*(3700 - 2790)
                    })
                }
            });
        }

        function unpause(){
            //allows the player to move again
            paused = false;

            //tracks guesses
            guessCount++;

            //gets rid of the menu
            menu.destroy();
            fruitBasket.destroy();
            vegetableBasket.destroy();
            pickedFruit.destroy();
            fruitLabel.destroy();

            //also destroys original sprite
            items[activeItemIndex].sprite.destroy();
            mapItems[activeItemIndex].sprite.destroy();

            itemsLeft--;
            itemsLeftText.text = "Items remaining: " + itemsLeft;

            //voice stuff.. TODO, fix robot showing up in weird places
            //also decides if response was right or wrong. Adds to a count if right.
            var correct = (guess == FRUIT && items[activeItemIndex].isFruit) || (guess==VEGETABLE && !items[activeItemIndex].isFruit);
            if(correct){
                correctCount++;
                correctAnswers.text = "Correct answers: " + correctCount;
                voiceService.speak("Correct! The " + items[activeItemIndex].plantName + " is a " + (items[activeItemIndex].isFruit? 'fruit!' : 'vegetable'), 0.9, 1);
            } else {
                voiceService.speak("OH NO! The " + items[activeItemIndex].plantName + " is a " + (items[activeItemIndex].isFruit? 'fruit!' : 'vegetable'), 0.9, 0.9);

            }


            //Record the answer given by the student
            answers.push({
                description: "Is " + items[activeItemIndex].plantName + " a fruit or a vegetable?",
                studentChoice: (guess==FRUIT? "Fruit":"Vegetable"),
                correctChoice: (items[activeItemIndex].isFruit? "Fruit":"Vegetable"),
                chapter: 6,
                choices: ["Fruit", "Vegetable"]
            });

            //checks to see if the game is over
            if(guessCount >= items.length){
                endGame();
            }
        }

        function update(){
            player.body.setZeroVelocity();
            mapPlayer.cameraOffset.x = player.position.x/15.36 + (game.camera.width - 300);
            mapPlayer.cameraOffset.y = player.position.y/15.36 + (game.camera.height - 300);

            if ((up || cursors.up.isDown) && paused == false)
            {
                player.body.moveUp(500)
            }
            else if ((down || cursors.down.isDown) && paused == false)
            {
                player.body.moveDown(500);
            }

            if ((left || cursors.left.isDown) && paused == false)
            {
                player.body.velocity.x = -500;
                player.animations.play('left');
            }
            else if ((right || cursors.right.isDown) && paused == false)
            {
                player.animations.play('right');
                player.body.moveRight(500);
            }
            else {
                //player.animations.stop();
                player.frame = 4;
            }
        }

        function render() {

            game.debug.cameraInfo(game.camera, 32, 32);
            game.debug.spriteCoords(player, 32, 500);

        }

        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        function endGame(){
            window.setTimeout(function(){
                voiceService.speak("Good job!");
                currentTestService.completeFruitVeggie();
                //game is over!  Go back to lobby
                navService.goToGameLobby();

            }, 3000);
            //Record the queued answers
            if(currentTestService.status.fruit_veggie==false){
                _.forEach(answers, function(answer){
                    currentTestService.recordAnswer(
                        answer.description, //description of the question
                        answer.studentChoice, //the student's choice
                        answer.correctChoice, //the correct choice
                        answer.chapter, // chapter in the JMG curriculum
                        answer.choices //array of strings listing all potential choices
                    );
                });
            }

        }

    }]);