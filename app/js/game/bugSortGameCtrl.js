app.controller('bugSortGameController', ['$scope', '$state', '$rootScope', 'currentTestService', 'gameService', 'imageService', 'pubSubService', 'voiceService', 'navService', 'settings',
    function($scope, $state, $rootScope, currentTestService, gameService, imageService, pubSubService, voiceService, navService, settings){

        var game = gameService.startNewGame(preload, create, update, 'game-container');
        var answers = [];
        var phaser_dude = null;
        var cursors;

        //manually selected coordinates... all may or may not be used. Need to add more unless random placement function is created
        var allBugs = JSON.parse(JSON.stringify(currentTestService.getCurrentAssessment().bugs)); //all the bugs in the current assessment
        var items = shuffle(
            _.filter(allBugs, function(bug){
                return bug.bugType != 3;
            })
        );
        var coordinates = [];
        //Build random coordinate pairs

        var mapItems = [];
        var walls;
        var player;
        var menu;
        var pooperBasket, pollinatorBasket, predatorBasket;
        var paused = false;
        var pickedBug;
        var bugLabel;
        var activeItemIndex;
        var guess;
        var PREDATOR = 0; var POLLINATOR = 1; var POOPER = 2; //guess values
        var guessCount = 0;
        var correctCount = 0;
        var correctAnswers;
        var itemsLeftText;
        var itemsLeft = items.length;

        var mapPlayer;

        var up = false;
        var down = false;
        var right = false;
        var left = false;

        var exitButtonName = null;

        function preload(){
            game.load.crossOrigin = 'anonymous';  //allows CORS requests to CDN
            //game.load.image('bg','app/assets/images/grassBackground.jpg');
            game.load.image('bg','app/assets/images/davidBackdrop.png');
            game.load.spritesheet('phaser_dude','app/assets/images/phaser_dude.png',32,48);
            walls = game.load.image('walls','app/assets/images/walls.png');
            game.load.physics('walls', 'app/assets/walls.json');
            game.load.image('menu','app/assets/images/menu.png');
            //game.load.image('fruits','app/assets/images/fruit.png');
            //game.load.image('veggies','app/assets/images/veggie.png');
            game.load.image('pooperBasket','app/assets/images/pooperBasket.png');
            game.load.image('predatorBasket','app/assets/images/predatorBasket.png');
            game.load.image('pollinatorBasket','app/assets/images/pollinatorBasket.png');
            game.load.image('map','app/assets/images/map.png');
            game.load.image('mapPlayer','app/assets/images/mapPlayer.png');
            game.load.image('mapItem','app/assets/images/mapItem.png');

            game.load.image('itemsLeftBackground','app/assets/images/itemsLeftBackground.png');

            exitButtonName = gameService.loadImage(game, 'exitBtn', 'app/assets/images/UI/exit.png');

            game.load.spritesheet('button-vertical', 'app/assets/images/button-vertical.png');
            game.load.spritesheet('button-horizontal', 'app/assets/images/button-horizontal.png');
            //load bug sprites
            items.forEach(function(bug){
                if(bug.images.length){
                    var uri, width, height;
                    var imageToUse = imageService.imageGetters.getBugSortImage(bug);
                    if (imageToUse.isSheet) {
                        uri = imageToUse.imageUri;
                        width = imageToUse.imageWidth / imageToUse.horizontalFrames;
                        height = imageToUse.imageHeight / imageToUse.verticalFrames;
                    }
                    if (!uri) { //if no image was found, fall back to a stock image
                        console.warn("Selected image is not a spritesheet.  Falling back to wasp image");
                        uri = 'app/assets/spritesheets/wasp_sprite.png';
                        width = 100;
                        height = 100;
                    }
                    gameService.loadSpriteSheet(game, {name:bug.bugName, path: uri, width:width, height:height});
                }
            });

            voiceService.preloadRobot(game);
        }

        function create(){
            game.add.tileSprite(0, 0, 3840, 3840, 'bg');
            walls = game.add.sprite(1904, 1904, 'walls');
            player = game.add.sprite(1920, 1920, 'phaser_dude');
            player.scale.setTo(2, 2);

            voiceService.createRobot(game);

            game.world.setBounds(0, 0, 3840, 3840);

            game.physics.startSystem(Phaser.Physics.P2JS);
            game.physics.p2.enable(player);
            player.body.fixedRotation = true;
            game.physics.p2.setImpactEvents(true);
            game.physics.p2.enable(walls);
            walls.enableBody = true;
            player.enableBody = true;
            walls.body.static = true;
            walls.body.clearShapes();
            walls.body.loadPolygon('walls','walls');

            //define collision groups
            var itemGroup = game.physics.p2.createCollisionGroup();
            var playerGroup = game.physics.p2.createCollisionGroup();
            var wallGroup = game.physics.p2.createCollisionGroup();

            setCoordinates();

            player.animations.add('left', [0, 1, 2, 3], 10, true);
            player.animations.add('right', [5, 6, 7, 8], 10, true);

            //does all the work for everything in the 'items' array (all bugs)
            items.forEach(function(item,index){

                //console.log(item.bugName+ "is placed at " + coordinates[index].x + ", " + coordinates[index].x);

                item.sprite = gameService.addSpriteUnscaled(game, coordinates[index].x, coordinates[index].y, 55, null, item.bugName, true);
                game.physics.p2.enable(item.sprite);
                game.physics.p2.enableBody(item.sprite);
                item.sprite.body.setCollisionGroup(itemGroup);
                item.sprite.body.collides(playerGroup);
                item.sprite.body.static = true;

                //create the bug sprite and put it at the starting location of the path
                item.sprite.animations.add('move');
                item.sprite.play('move', 15, true);
            });

            var map = game.add.sprite(game.camera.x+game.camera.width-300, game.camera.y+game.camera.height - 300, 'map');
            map.fixedToCamera = true;

            items.forEach(function(item,index) {
                var mapItem = {
                    sprite: game.add.sprite(0, 0, 'mapItem'),
                    plantName: item.plantName
                };
                mapItems.push(mapItem);
                mapItem.sprite.fixedToCamera = true;
                mapItem.sprite.anchor.setTo(0.5,0.5);
                mapItem.sprite.cameraOffset.x = item.sprite.position.x/15.36 + (game.camera.width - 300);
                mapItem.sprite.cameraOffset.y = item.sprite.position.y/15.36 + (game.camera.height - 300);
            });

            player.body.collides(itemGroup);

            player.body.setCollisionGroup(playerGroup);
            walls.body.setCollisionGroup(wallGroup);
            player.body.collides(wallGroup);
            walls.body.collides(playerGroup);

            game.physics.p2.updateBoundsCollisionGroup();

            mapPlayer = game.add.sprite(game.camera.x+game.camera.width-300+ 137, game.camera.y+game.camera.height - 300 + 137, 'mapPlayer')
            mapPlayer.fixedToCamera = true;
            mapPlayer.anchor.setTo(0.5, 0.5);

            var ilbg = game.add.sprite(game.camera.x + 25, game.camera.y + 25, 'itemsLeftBackground');
            ilbg.fixedToCamera = true;

            itemsLeftText = game.add.text( game.camera.x + 44, game.camera.y + 37, "Items remaining: " + itemsLeft, { font: '24px Arial', fill: '#fff' });
            itemsLeftText.fixedToCamera = true;

            var cabg = game.add.sprite(game.camera.x + game.camera.width - 279, game.camera.y + 25, 'itemsLeftBackground');
            cabg.fixedToCamera = true;

            correctAnswers = game.add.text( game.camera.x + game.camera.width - 260, game.camera.y + 37, "Correct answers: " + correctCount, { font: '24px Arial', fill: '#fff' });
            correctAnswers.fixedToCamera = true;

            var buttonleft = game.add.button(game.camera.x, game.camera.y+game.camera.height - 128-64, 'button-horizontal', null, this, 0, 1, 0, 1);
            buttonleft.fixedToCamera = true;
            buttonleft.events.onInputDown.add(function(){left=true;});
            buttonleft.events.onInputUp.add(function(){left=false;});

            var buttonright = game.add.button(game.camera.x+128+64,game.camera.y+game.camera.height - 128-64 , 'button-horizontal', null, this, 0, 1, 0, 1);
            buttonright.fixedToCamera = true;
            buttonright.events.onInputDown.add(function(){right=true;});
            buttonright.events.onInputUp.add(function(){right=false;});

            var buttondown = game.add.button(game.camera.x + 128, game.camera.y+game.camera.height - 128, 'button-vertical', null, this, 0, 1, 0, 1);
            buttondown.fixedToCamera = true;
            buttondown.events.onInputDown.add(function(){down=true;});
            buttondown.events.onInputUp.add(function(){down=false;});

            var buttonup = game.add.button(game.camera.x + 128, game.camera.y+game.camera.height - 128-64-128, 'button-vertical', null, this, 0, 1, 0, 1);
            buttonup.fixedToCamera = true;
            buttonup.events.onInputDown.add(function(){up=true;});
            buttonup.events.onInputUp.add(function(){up=false;});

            //Show the exit/abort button
            var exit = gameService.addSprite(game, 750, 550, 25, null, exitButtonName);
            exit.fixedToCamera = true;
            exit.inputEnabled = true;
            exit.events.onInputDown.add(function(){
                if(prompt("Are you sure you wish to abort?  The student's progress will not be saved.\n" +
                        "Please enter the PIN to continue") == settings.pin){
                    //exit without saving answers
                    navService.goToGameLobby();
                }
            }, this);

            voiceService.speak('Oh no!  The good bugs have been scattered all over your house.  Move around and sort the good bugs into the right baskets.', 0.9, 1);

            //happens when the player touches a bug
            player.body.onBeginContact.add(function (bodyA,bodyB,shapeA,shapeB,equation) {
                if(bodyA.sprite.key != 'walls' && !paused) {

                    //prevents player movement until choice is made
                    paused = true;

                    //finds the item in the 'items' arrday
                    items.forEach(function(bug,index){
                        if(bug.bugName == bodyA.sprite.key){
                            activeItemIndex = index;
                        }
                    });

                    //creates the menu
                    menu = gameService.addSprite(game, 400, 300, 600, 400, 'menu', true);
                    //menu = game.add.sprite(game.camera.width / 2, game.camera.height / 2, 'menu');
                    //menu.anchor.setTo(0.5, 0.5);
                    menu.fixedToCamera = true;

                    //Add the pooper basket
                    pooperBasket = gameService.addSprite(game, 200, 400, null, 150, 'pooperBasket', true);
                    pooperBasket.fixedToCamera = true;
                    pooperBasket.inputEnabled = true;
                    pooperBasket.events.onInputUp.add(function(){
                        guess = POOPER;
                        unpause();
                    });

                    //Add the pollinator basket
                    pollinatorBasket = gameService.addSprite(game, 400, 400, null, 150, 'pollinatorBasket', true);
                    pollinatorBasket.fixedToCamera = true;
                    pollinatorBasket.inputEnabled = true;
                    pollinatorBasket.events.onInputUp.add(function(){
                        guess = POLLINATOR;
                        unpause();
                    });

                    //Add the Predator basket
                    predatorBasket = gameService.addSprite(game, 600, 400, null, 150, 'predatorBasket', true);
                    predatorBasket.fixedToCamera = true;
                    predatorBasket.inputEnabled = true;
                    predatorBasket.events.onInputUp.add(function(){
                        guess = PREDATOR;
                        unpause();
                    });

                    //display the bug the player touched
                    pickedBug = game.add.sprite(game.camera.width/2,game.camera.height/2 - 90,items[activeItemIndex].bugName);
                    pickedBug.aspectRatio = pickedBug.width/pickedBug.height;
                    pickedBug.height = game.camera.height/5;
                    pickedBug.width = pickedBug.aspectRatio * pickedBug.height;
                    pickedBug.anchor.setTo(0.5, 0.5);
                    pickedBug.fixedToCamera = true;

                    //shows the name of the bug
                    bugLabel = gameService.addText(game, 400, 125, null, 25,  items[activeItemIndex].bugName.toUpperCase(), 40, '#ffffff');
                    //bugLabel = game.add.text( game.camera.width/2, game.camera.height/3, items[activeItemIndex].bugName.toUpperCase().replace(/ /g, '\n'), { font: '40px Arial', fill: '#fff' });
                    //bugLabel.anchor.set(0.5);
                    bugLabel.fixedToCamera = true;
                }
            });

            cursors = game.input.keyboard.createCursorKeys();

            game.camera.follow(player);
        }

        function setCoordinates(){
            items.forEach(function(item){
                var sector = Math.random()*4 + 1;
                if(sector <= 2){
                    coordinates.push({
                        x: 150 + Math.random()*3540,
                        y: 200 + Math.random()*900
                    })
                } else if (sector <= 3) {
                    coordinates.push({
                        x: 150 + Math.random()*1350,
                        y: 1280 + Math.random()*(2650-1280)
                    })
                } else if (sector <= 4) {
                    coordinates.push({
                        x: 2280 + Math.random()*(3700-2280),
                        y: 1280 + Math.random()*(2650-1280)
                    })
                } else if (sector <= 5) {
                    coordinates.push({
                        x: 150 + Math.random()*3540,
                        y: 2790 + Math.random()*(3700 - 2790)
                    })
                }
            });
        }

        function unpause(){
            //allows the player to move again
            paused = false;

            //tracks guesses
            guessCount++;

            //gets rid of the menu
            menu.destroy();
            pooperBasket.destroy();
            pollinatorBasket.destroy();
            predatorBasket.destroy();
            pickedBug.destroy();
            bugLabel.destroy();

            //also destroys original sprite
            items[activeItemIndex].sprite.destroy();
            mapItems[activeItemIndex].sprite.destroy();

            itemsLeft--;
            itemsLeftText.text = "Items remaining: " + itemsLeft;

            //also decides if response was right or wrong. Adds to a count if right.
            var correct = (guess == items[activeItemIndex].bugType);
            if(correct){
                correctCount++;
                correctAnswers.text = "Correct answers: " + correctCount;
                voiceService.speak("Correct! The " + items[activeItemIndex].bugName + " is a " + bugTypeToText(items[activeItemIndex].bugType), 0.9, 1);
            } else {
                voiceService.speak("OH NO! The " + items[activeItemIndex].bugName + " is a " + bugTypeToText(items[activeItemIndex].bugType) + ", not a " + bugTypeToText(guess), 0.9, 0.9);
            }

            answers.push({
                description: "Is " + items[activeItemIndex].bugName + " a predator, pollinator, or pooper?",
                studentChoice: bugTypeToText(guess),
                correctChoice: bugTypeToText(items[activeItemIndex].bugType),
                chapter: 4,
                choices: ["Predator", "Pollinator", "Pooper"] //choices
            });

            //checks to see if the game is over
            if(guessCount >= items.length){
                endGame();
            }
        }

        function update(){
            player.body.setZeroVelocity();
            mapPlayer.cameraOffset.x = player.position.x/15.36 + (game.camera.width - 300);
            mapPlayer.cameraOffset.y = player.position.y/15.36 + (game.camera.height - 300);

            if ((up || cursors.up.isDown) && paused == false)
            {
                player.body.moveUp(500)
            }
            else if ((down || cursors.down.isDown) && paused == false)
            {
                player.body.moveDown(500);
            }

            if ((left || cursors.left.isDown) && paused == false)
            {
                player.body.velocity.x = -500;
                player.animations.play('left');
            }
            else if ((right || cursors.right.isDown) && paused == false)
            {
                player.animations.play('right');
                player.body.moveRight(500);
            }
            else {
                player.frame = 4;
            }
        }

        function render() {
            game.debug.cameraInfo(game.camera, 32, 32);
            game.debug.spriteCoords(player, 32, 500);
        }

        function bugTypeToText(typeNum){
            if(typeNum==PREDATOR){
                return "Predator";
            } else if(typeNum==POOPER){
                return "Pooper";
            } else if(typeNum==POLLINATOR){
                return "Pollinator";
            } else{
                return "Pest";
            }
        }

        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        function endGame(){

            //Record the queued answers
            if(currentTestService.status.bug_sort==false){
                _.forEach(answers, function(answer){
                    currentTestService.recordAnswer(
                        answer.description, //description of the question
                        answer.studentChoice, //the student's choice
                        answer.correctChoice, //the correct choice
                        answer.chapter, // chapter in the JMG curriculum
                        answer.choices //array of strings listing all potential choices
                    );
                });
            }

            window.setTimeout(function(){
                voiceService.speak("Good job!  You found all the bugs!");
                currentTestService.completeBugSort();
                //game is over!  Go back to lobby
                navService.goToGameLobby();

            }, 2000);
        }

        var currentState = $state.current.name;
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if(toState.name != currentState){
                    if(game){
                        game.destroy();
                        game = null;
                    }
                }
            });

    }]);