//TODO this game calls "getBugs" everytime an answer is given, which is bad!
//TODO Remove old images when runGame is called

app.controller("bugMatchGameController", ['$scope', '$rootScope', '$state', 'gameService', 'bugService', 'imageService', 'voiceService',
    'currentTestService', 'dimensionsService', 'navService', 'settings',
    function($scope, $rootScope, $state, gameService, bugService, imageService, voiceService,
             currentTestService, dimensionsService, navService, settings) {
        $scope.error = null;

        $scope.openModal = function(){
            $('#loadModal').openModal();
        };
        $scope.closeModal = function(){
            $('#loadModal').closeModal();
        };

        $scope.gameCompleted = currentTestService.status.bug_match;

        var game = gameService.startNewGame(preload, create, update, 'game-container');
        var answers = [];

        var expectedWidth = 800;
        var expectedHeight = 600;

        var width = dimensionsService.getWidth();
        var height = dimensionsService.getHeight();

        var exitButtonName = null;

        var gameControls = null;  // This will later be an object with controls to manipulate bugs and text
        //These will be changed as the game progresses
        $scope.correctBug = null;
        $scope.correctBugIndex = null;
        $scope.otherBugs = null;
        $scope.chosenBug = null;
        $scope.chosenBugIndex = null;


        function preload() {
            game.load.crossOrigin = 'anonymous';  //allows CORS requests to CDN
            $scope.openModal();
            //Here we load all assets for the game
            $scope.assessmentBugs = JSON.parse(JSON.stringify(currentTestService.getCurrentAssessment().bugs));

            exitButtonName = gameService.loadImage(game, 'exitBtn', 'app/assets/images/UI/exit.png');

            $scope.allBugs = JSON.parse(JSON.stringify(currentTestService.getCurrentAssessment().bugs));
            game.load.image("background", "app/assets/images/backdrop.png");
            voiceService.preloadRobot(game);
            //load image for each bug
            _.forEach($scope.allBugs, function (bug) {
                if(bug.images.length){
                    game.load.image(bug.bugName, imageService.imageGetters.getBugMatchImage(bug).imageUri);
                }
            });
        }

        function create() {
            //Here we create game assets and position them on screen
            if (!$scope.error) {
                //Create background
                var bg = game.add.sprite(0, 0, "background");
                bg.scale.x = width / expectedWidth;
                bg.scale.y = height / expectedHeight;

                setupGame();
            }

            var exit = gameService.addSprite(game, 750, 550, 25, null, exitButtonName);
            exit.inputEnabled = true;
            exit.events.onInputDown.add(function(){
                //console.log("INPUT DOWN!");
                if(prompt("Are you sure you wish to abort?  The student's progress will not be saved.\n" +
                        "Please enter the PIN to continue") == settings.pin){
                    //exit without saving answers
                    navService.goToGameLobby();
                }
            }, this);
        }

        function update() {
            //This function gets called every frame so we can check for events, update positions, etc
        }

        function setupGame() {
            $scope.totalQuestionsAsked = 0;
            $scope.totalCorrect = 0;


            /// CREATING A ROBOT
            var initialMsg = "Hello human. " +
                "Please help me ... ";
            voiceService.createRobot(game);
            voiceService.speakWithRobot(initialMsg, 0.9, 1,
                voiceService.robots.NEUTRAL, voiceService.robotLocations.BOTTOMRIGHT);


            //Create text overlays for the currentInstruction and current score
            var instruction = game.add.text(width * 0.03, height * 0.03, "FInd the ", {
                fontSize: Math.max(height * 32 / expectedHeight, 12),
                fill: '#FFFFFF'
            }); //white


            var score = game.add.text(width * 0.92, height * 0.03, "0/0", {
                fontSize: Math.max(height * 32 / expectedHeight, 12),
                fill: '#FFFFFF'
            });

            var feedback = game.add.text(width * 0.03, height * 0.8, "", {
                fontSize: Math.max(height * 32 / expectedHeight, 12),
                fill: '#FFFFFF'
            });

            var boxWidth = width * 0.30;
            var boxHeight = height * 0.30;
            var boxAspectRatio = (width * 0.30) / (height * 0.30);

            var bugs = [null, null, null, null]; // will later hold the 4 bugs

            //This object allows us to modify the text and the four bugs and such in a black-box way without creating a bunch of global vars
            gameControls = {
                updateInstruction: function (text) {
                    instruction.text = text;
                    voiceService.speakWithRobot(text, 0.9, 1,
                        voiceService.robots.NEUTRAL, voiceService.robotLocations.BOTTOMRIGHT);

                },
                updateScore: function (text) {
                    score.text = text;
                },
                updateFeedback: function (feedbackMessage) {
                    feedback.text = feedbackMessage;
                },
                clearFeedback: function () {
                    feedback.text = "";
                },
                speakMessage: function (message) {
                    voiceService.speakWithRobot(message, 0.9, 1,
                        voiceService.robots.NEUTRAL, voiceService.robotLocations.BOTTOMRIGHT);
                },
                updateBug: function (index, bugName) {
                    var i = index - 1;
                    if (bugs[i]) {
                        bugs[i].destroy();
                    }
                    var xPos = width * 0.15 + (i % 2) * (width * 0.35);
                    var yDown = (i >= 2) ? 1 : 0;
                    var yPos = height * 0.15 + yDown * (height * 0.35);
                    var bug = game.add.sprite(xPos, yPos, bugName);
                    var bugAR = bug.width / bug.height;
                    if (bugAR > boxAspectRatio) {
                        //scale bug to fit in width
                        bug.width = boxWidth;
                        bug.height = bug.width / bugAR;
                        //center bug vertically
                        var dy = boxHeight + yPos - (bug.y + bug.height);
                        bug.y = yPos + 0.5 * dy;
                    }
                    else {
                        //scale bug to fit in height
                        bug.height = boxHeight;
                        bug.width = bug.height * bugAR;
                        //center bug horizontally
                        var dx = boxWidth + xPos - (bug.x + bug.width);
                        bug.x = xPos + 0.5 * dx;
                    }

                    //attach tap controller:
                    bug.inputEnabled = true;
                    bug.events.onInputDown.add(function () {
                        chooseBug(index);
                    }, bug);
                    bugs[i] = bug;
                }
            };

            //Kick things off!
            $scope.closeModal();
            takeStep();

        }

        //Takes a "step" in the game.  Takes bug from queue, changes text, and loads more bugs
        function takeStep(){
            if($scope.assessmentBugs.length){
                //do normal actions
                //get next correct bug randomly from queue
                var correctBug = $scope.assessmentBugs.splice(_.random($scope.assessmentBugs.length - 1), 1)[0];
                var index = Math.floor(4 * Math.random()) + 1;  // between 1 and 4
                gameControls.updateBug(index, correctBug.bugName);
                $scope.correctBug = correctBug;
                $scope.correctBugIndex = index;

                //get other bugs from set of all other bugs
                var otherBugs = _.filter($scope.allBugs, function(bug){
                    return bug.bugName != correctBug.bugName;
                });
                var others = _.sampleSize(otherBugs, 3);
                for(var i = 0; i < others.length; i++){
                    var indx = (i >= index-1)? i+1 : i;
                    gameControls.updateBug(indx+1, others[i].bugName);
                }
                $scope.otherBugs = others;
                gameControls.updateInstruction("Find the " + correctBug.bugName);

            }
            else{
                //Record all the answers to currentTestService
                if(!$scope.gameCompleted){
                    _.forEach(answers, function(answer){
                        currentTestService.recordAnswer(
                            answer.description, //description of the question
                            answer.studentChoice, //the student's choice
                            answer.correctChoice, //the correct choice
                            answer.chapter, // chapter in the JMG curriculum
                            answer.choices //array of strings listing all potential choices
                        );
                    });
                }
                //game is over!  Go back to lobby
                currentTestService.completeBugMatch();
                navService.goToGameLobby();

            }

        }

        function chooseBug(index){
            $scope.totalQuestionsAsked += 1;
            //called when student taps a bug at some index
            $scope.chosenBugIndex = index;
            //index will be a number 1 through 4
            var correct = index==$scope.correctBugIndex;

            if(correct){

                gameControls.updateFeedback("Correct! That is the " + $scope.correctBug.bugName);

                $scope.totalCorrect += 1;
                $scope.chosenBug = $scope.correctBug;

                voiceService.speakWithRobot("Correct! That is the " + $scope.correctBug.bugName, 0.9, 1,
                    voiceService.robots.CORRECT, voiceService.robotLocations.BOTTOMRIGHT);
            }
            else{
                var indx = (index >=$scope.correctBugIndex)? index - 2 : index -1;
                $scope.chosenBug = $scope.otherBugs[indx];
                //gameControls.speakMessage("Incorrect! That is the " + $scope.chosenBug.bugName);

                voiceService.speakWithRobot("Incorrect. That is the " + $scope.chosenBug.bugName, 0.9, 1,
                    voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMRIGHT);

                gameControls.updateFeedback("Incorrect. That is the " + $scope.chosenBug.bugName);
            }

            //update score
            var score = "" + $scope.totalCorrect + "/" + $scope.totalQuestionsAsked;
            gameControls.updateScore(score);


            var choices = [$scope.correctBug.bugName];
            _.forEach($scope.otherBugs, function(bug){
                choices.push(bug.bugName);
            });

            answers.push({
                description: "Find the " + $scope.correctBug.bugName,
                studentChoice: $scope.chosenBug.bugName,
                correctChoice: $scope.correctBug.bugName,
                chapter: 1,
                choices: choices
            });

            takeStep();
        }

        var currentState = $state.current.name;
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if(toState.name != currentState){
                    if(game && game.destroy){
                        game.destroy();
                        game = null;
                    }
                }
            });
    }]);


