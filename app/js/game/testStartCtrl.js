// Controller to setup the Seed Packet game
//
app.controller("testStartController", ['$scope', '$log', 'studentService', 'assessmentService', 'navService', 'currentTestService', 'gameSettingsService',
function($scope, $log, studentService, assessmentService, navService, currentTestService, gameSettingsService){
    $scope.students = [];
    $scope.assessments = [];

    $scope.settings = {};

    $scope.data = {
        selectedStudent: null,
        selectedAssessment: null
    };

    $scope.loading = false;
    $scope.error = false;

    $scope.loadStudents = function(){
        $scope.loading = true;
        $scope.error = false;

        return studentService.getStudents()
            .then(function(students){
                $scope.students = students;
            }, function(error){
                $log.error(JSON.stringify(error));
                $scope.error = true;
            })
            .finally(function(){
                $scope.loading = false;
            })
    };

    $scope.loadAssessments = function(){
        $scope.loading = true;
        $scope.error = false;

        return assessmentService.getAll()
            .then(function(assessments){
                $scope.assessments = assessments;
            }, function(error){
                $log.error(JSON.stringify(error));
                $scope.error = true;
            })
            .finally(function(){
                $scope.loading = false;
            })
    };

    $scope.loadSettings = function(){
        gameSettingsService.getAll()
            .then(function(data){
                $scope.settings = data[0];
            }, function(error){
                $scope.error = "Could not load game settings";
            })
            .finally(function(){
                $scope.loading = false;
            })
    };

    //setup
    $scope.loadStudents()
        .then($scope.loadAssessments)
        .then($scope.loadSettings);

    $scope.startGame = function(){
        currentTestService.startNewTest($scope.data.selectedStudent, $scope.data.selectedAssessment);

        navService.startGame();
    };

}]);