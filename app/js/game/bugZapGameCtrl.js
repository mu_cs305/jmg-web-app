//Controller to setup and run the bug zap game

app.controller('bugZapGameController', ['$scope', '$rootScope', 'currentTestService', 'gameService', 'imageService', 'pubSubService', 'voiceService', 'navService', '$state', 'settings',
    function($scope, $rootScope, currentTestService, gameService, imageService, pubSubService, voiceService, navService, $state, settings){
        var assessment = currentTestService.getCurrentAssessment();
        var bugs = JSON.parse(JSON.stringify(assessment.bugs));
        var pestCount = 0;
        var pestArray = [];
        var goodBugArray = [];
        var goodBugCount = 0;
        _.forEach(bugs, function(bug){
            if(bug.bugType == 3){
                pestArray[pestCount] = bug;
                pestCount++;
            }

            else
            {
                goodBugArray[goodBugCount] = bug;
                goodBugCount++;
            }

        });
        var responseCount = 0;
        var answers = [];  // will hold students answers before they are flushed to the currentTestService

        //variables to keep track of game stuff like spritesheets and sprites
        var game = gameService.startNewGame(preload, create, update, 'game-container');
        var backgroundName = null;
        var exitButtonName = null;
        var spriteSheetNames = null;
        var bugSheetNames = null;
        var bugSprites = {};
        var tank = null;

        //variables to control the missile
        var missileName = null;
        var missileSprite = null;
        var missileActive = false;
        var missileTarget = null;
        var missileTargetBug = null; //bug object targeted by the missile
        var explosionSprite = null;
        var badBugNumber = 0;
        var goodBugNumber = 0;
        var firstBugsCount = true;

        //The preload function is used to load assets and get the game ready
        function preload(){
            game.load.crossOrigin = 'anonymous';  //allows CORS requests to CDN
            //load misc images like background and the missile
            backgroundName = gameService.loadImage(game, 'bg', 'app/assets/images/backdrop.png');
            missileName = gameService.loadImage(game, 'missile', 'app/assets/images/rocket.png');

            exitButtonName = gameService.loadImage(game, 'exitBtn', 'app/assets/images/UI/exit.png');
            //load sprite sheets
            spriteSheetNames = gameService.loadSpriteSheets(game, [
                {name:'tank', path:'app/assets/spritesheets/gun3_sheet.png', width:128, height:128},
                {name:'explosion', path:'app/assets/spritesheets/explosion.png', width:128, height:128}
            ]);
            //load sprite sheets for each bug
            var bugSheets = [];
            _.forEach(bugs, function(bug){
                var uri, width, height;
                var imageToUse = imageService.imageGetters.getBugSortImage(bug);
                if (imageToUse.isSheet) {
                    uri = imageToUse.imageUri;
                    width = imageToUse.imageWidth / imageToUse.horizontalFrames;
                    height = imageToUse.imageHeight / imageToUse.verticalFrames;
                }
                if (!uri) { //if no image was found, fall back to a stock image
                    console.warn("Selected image is not a spritesheet.  Falling back to wasp image");
                    uri = 'app/assets/spritesheets/wasp_sprite.png';
                    width = 100;
                    height = 100;
                }
                bugSheets.push({name:bug.bugName, path: uri, width:width, height:height});
            });
            bugSheetNames = gameService.loadSpriteSheets(game, bugSheets);

            //load the robot for the voice service
            voiceService.preloadRobot(game);

        }

        function create(){
            //Throw up the background image
            var bg = gameService.addSprite(game, 0, 0, 800, 600, backgroundName);
            //Show the tank at the bottom of the screen
            tank = gameService.addSprite(game, 400, 520, 80, null, spriteSheetNames[0]);
            //Show the exit/abort button
            var exit = gameService.addSprite(game, 750, 50, 25, null, exitButtonName);
            exit.inputEnabled = true;
            exit.events.onInputDown.add(function(){
                if(prompt("Are you sure you wish to abort?  The student's progress will not be saved.\n" +
                    "Please enter the PIN to continue") == settings.pin){
                    //exit without saving answers
                    navService.goToGameLobby();
                }
            }, this);
            //Load the missile but keep it offscreen for now
            missileSprite = gameService.addSprite(game, -1600, -1600, 18, null, missileName, true);
            missileSprite.anchor.setTo(0.5, 1);
            tank.anchor.setTo(0.5, 1);
            //load the explosion and keep it offscreen
            explosionSprite = gameService.addSprite(game, -1600, -1600, 40, null, spriteSheetNames[1], true);
            explosionSprite.animations.add('explode');


            //Load bugs and create them randomized paths


                //Randomly generate a path for the bug to follow - hopefully these are fun!!

            //Selects a random "bad bug"
            badBugNumber = Math.floor((Math.random()*(pestArray.length-1)));
                var pathX = [];
                var pathY = [];
                for (var i = 0; i < 20; i++) {
                    pathX.push(700 * Math.random());
                    pathY.push(500 * Math.random());
                }
                //Make sure that the bug winds up where it starts
                pathX.push(pathX[0]);
                pathY.push(pathY[0]);

                createBugs();

            //create the robot for the voice service
            voiceService.createRobot(game);

            //once this is done, we'll give them instructions
            window.setTimeout(function(){
                voiceService.speakWithRobot("OH NO! Some mean pests have invaded!.  Zap the pests..  Make sure to not blow up the good bugs!", 0.9, 0.9,
                    voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMRIGHT);
                     }, 2500);

            //First run through the explanation/which bug is the bad bug. This prevents interuption from the "OH NO" line. Randomizer for order differentiation.
            var coinFlip = Math.floor(Math.random() * 2);
            if (coinFlip == 1) {


                window.setTimeout(function () {
                    voiceService.speakWithRobot("         Blow Up the Pest! Is the pest the " + goodBugArray[goodBugNumber].bugName + " or the " + pestArray[badBugNumber].bugName, 0.9, 0.9,
                        voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMRIGHT);
                }, 2500);


            }
            else {

                window.setTimeout(function () {
                    voiceService.speakWithRobot("       Blow up the Pest! Is the pest the " + pestArray[badBugNumber].bugName + " or the " + goodBugArray[goodBugNumber].bugName, 0.9, 0.9,
                        voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMRIGHT);
                }, 2500);
            }

            firstBugsCount = false;
        }

        function update(){
            //animate bugs
            advanceBugs();
            advanceMissile(); //This will only happen if the missile is active
        }

        //this function will walk the bugs along their paths if the freezeBug indicator isn't set
        var maxVelocity = 3;  //in pixels per frame
        function advanceBugs(){


            _.forEach(bugSprites, function(bug){
                if(!bug.frozen && bug.path){
                    if(bug.pathCounter>=bug.path.length){
                        bug.pathCounter = 0;
                    }
                    var dx = bug.path[bug.pathCounter].x-bug.x;
                    var dy = bug.path[bug.pathCounter].y - bug.y;
                    var angle = bug.path[bug.pathCounter].angle;
                    var dist = differentialDistance(dx, dy);
                    if(dist >= maxVelocity){
                        //scale dx and dy so that velocity is regulated
                        var scaleFactor = maxVelocity/dist;
                        dx = scaleFactor*dx;
                        dy = scaleFactor*dy;
                    } else{
                        bug.pathCounter++;
                    }
                    bug.x +=dx;
                    bug.y +=dy;
                    bug.rotation = angle;
                }
            })

        }

        var missileVelocity = 10;  //make sure this is higher than the bug velocity or else the missile will never catch its target
        function advanceMissile(){
            if(missileActive){
                var dx = missileTarget.x - missileSprite.x;
                var dy = missileTarget.y - missileSprite.y;
                var angle = game.math.angleBetweenPoints(
                    {x: missileSprite.x, y: missileSprite.y}, {x: missileTarget.x, y: missileTarget.y}) + Math.PI/2;
                var dist = differentialDistance(dx, dy);
                if(dist > missileVelocity){
                    var scale = missileVelocity/dist;
                    dx = dx*scale;
                    dy = dy*scale;
                }
                //advance missile
                missileSprite.x += dx;
                missileSprite.y += dy;
                missileSprite.rotation = angle;
                //detect if the missile has hit the bug and dispatch event if it did
                var hitZone = missileTarget.getBounds();
                hitZone.width = missileTarget.width/2;
                hitZone.height = missileTarget.height/2;
                if(Phaser.Rectangle.intersects(missileSprite.getBounds(), hitZone)){
                    pubSubService.publish('game/bugZap/missileStrike',
                        {missile: missileSprite, targetSprite: missileTarget});
                }
            }
        }

        //event listener for when a bug is clicked - will dispatch a missile to destroy the bug and records the student's answer
        function bugClicked(eventInfo){
            //respond to the event if a missile isn't already active
            if(!missileActive){
                missileTarget = eventInfo.sprite; //update missileTarget
                //move the missile into its original position
                missileSprite.x = tank.x;
                missileSprite.y = tank.y - tank.height;
                missileActive = true;
                missileTargetBug = eventInfo.bug;

                //record the student's answer (we only record the first n answers, where n is the number of pests for this particular game
                if(responseCount < pestCount){
                    // var choices = _.map(bugs, function(bug){return bug.bugName});
                    // var correctChoices = _.map(
                    //     _.filter(bugs, function(bug){return bug.bugType==3}),
                    //     function(bug){return bug.bugName}
                    // ).join(',');
                    answers.push({
                        description: "Zap the Pest",
                        studentChoice: missileTargetBug.bugName,
                        correctChoice: pestArray[badBugNumber].bugName,
                        chapter: 4,
                        choices: [pestArray[badBugNumber].bugName, goodBugArray[goodBugNumber].bugName]
                    });
                    responseCount++;
                }

            }
        }
        pubSubService.subscribe('game/bugZap/clicked', bugClicked);

        //event handler for when a missile collides with a bug
        function missileCollision(eventInfo){
            var missile = eventInfo.missile;
            //play the explosion here where the collision occurred
            explosionSprite.x = missileTarget.x; explosionSprite.y = missileTarget.y;
            explosionSprite.play('explode', 40, false);
            //make the missile inactive and move it off screen
            missileActive = false;
            missile.rotation = 0;
            missile.x = -1600; missile.y = -1600;
            var correctChoices = [];
            correctChoices = _.map(
                _.filter(bugs, function(bug){return bug.bugType==3}),
                function(bug){return bug.bugName}
            );

            var isCorrect = false;
            //Gives vocal feedback for correct/incorrect answer
            for (var i = 0 ; i <correctChoices.length; i++)
            {
                if (missileTargetBug.bugName == correctChoices[i])
                {

                    isCorrect = true;
                    voiceService.speakWithRobot("Correct! That was the "+missileTargetBug.bugName+", a pest.", 0.9, 1,
                        voiceService.robots.CORRECT, voiceService.robotLocations.BOTTOMRIGHT);

                    //remove the bugs
                    removeBug(missileTargetBug);
                    delete bugSprites[missileTargetBug.bugName];
                    missileTarget.destroy();
                    missileTarget = null;
                    missileTargetBug = null;


                    bugSprites[goodBugArray[goodBugNumber].bugName].destroy();
                    delete bugSprites[goodBugArray[goodBugNumber].bugName];

                    break;
                }
            }
            if(!isCorrect)
            {
                voiceService.speakWithRobot("OH NO! That was the "+missileTargetBug.bugName+", a friendly bug!", 0.9, 0.9,
                    voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMRIGHT);

                //remove the bugs
                removeBug(missileTargetBug);
                delete bugSprites[missileTargetBug.bugName];
                missileTarget.destroy();
                missileTarget = null;
                missileTargetBug = null;


                bugSprites[pestArray[badBugNumber].bugName].destroy();
                delete bugSprites[pestArray[badBugNumber].bugName];
            }

           // bugSprites[goodBugArray[goodBugNumber].bugName] = null;

            //check for game over
            checkGameOver();
            // createBugs();

        }
        pubSubService.subscribe('game/bugZap/missileStrike', missileCollision);

        function removeBug(bug){
            bugs.splice(bugs.indexOf(bug), 1);
        }

        function checkGameOver(){
            if(pestArray.length == 1) {
                pubSubService.publish('game/bugZap/gameover', {});
                return true;
            } else{
                createBugs();
                return false;
            }
        }

        function createBugs() {
            //Load bugs and create them randomized paths

            //Randomly generate a path for the bug to follow - hopefully these are fun!!
            pestArray.splice(badBugNumber, 1);
            //Selects a random "bad bug"
            badBugNumber = Math.floor((Math.random() * (pestArray.length - 1)));
            var pathX = [];
            var pathY = [];
            for (var i = 0; i < 20; i++) {
                pathX.push(700 * Math.random());
                pathY.push(500 * Math.random());
            }
            //Make sure that the bug winds up where it starts
            pathX.push(pathX[0]);
            pathY.push(pathY[0]);

            //create the bug sprite and put it at the starting location of the path
            bugSprites[pestArray[badBugNumber].bugName] = gameService.addSprite(game, pathX[0], pathY[0], null, 75, pestArray[badBugNumber].bugName, true);
            bugSprites[pestArray[badBugNumber].bugName].animations.add('move');
            bugSprites[pestArray[badBugNumber].bugName].play('move', 15, true);

            //Add the object properties needed to animate the bug along its path
            bugSprites[pestArray[badBugNumber].bugName].path = gameService.generatePath(game, pathX, pathY);
            bugSprites[pestArray[badBugNumber].bugName].frozen = false;
            bugSprites[pestArray[badBugNumber].bugName].pathCounter = 0;

            //cause the bug to dispatch event when it's clicked
            bugSprites[pestArray[badBugNumber].bugName].inputEnabled = true;
            bugSprites[pestArray[badBugNumber].bugName].events.onInputDown.add(function () {
                pubSubService.publish('game/bugZap/clicked', {
                    bug: pestArray[badBugNumber],
                    sprite: bugSprites[pestArray[badBugNumber].bugName]
                });
            }, this);


            //Randomly generate a path for the bug to follow - hopefully these are fun!!

            //Selects a random "good bug"
            var pathX = [];
            var pathY = [];
            for (var i = 0; i < 20; i++) {
                pathX.push(700 * Math.random());
                pathY.push(500 * Math.random());
            }
            //Make sure that the bug winds up where it starts
            pathX.push(pathX[0]);
            pathY.push(pathY[0]);

            //create the bug sprite and put it at the starting location of the path
            goodBugNumber = Math.floor((Math.random() * (goodBugArray.length - 1)));
            bugSprites[goodBugArray[goodBugNumber].bugName] = gameService.addSprite(game, pathX[0], pathY[0], null, 75, goodBugArray[goodBugNumber].bugName, true);
            bugSprites[goodBugArray[goodBugNumber].bugName].animations.add('move');
            bugSprites[goodBugArray[goodBugNumber].bugName].play('move', 15, true);

            //Add the object properties needed to animate the bug along its path
            bugSprites[goodBugArray[goodBugNumber].bugName].path = gameService.generatePath(game, pathX, pathY);
            bugSprites[goodBugArray[goodBugNumber].bugName].frozen = false;
            bugSprites[goodBugArray[goodBugNumber].bugName].pathCounter = 0;

            //cause the bug to dispatch event when it's clicked
            bugSprites[goodBugArray[goodBugNumber].bugName].inputEnabled = true;
            bugSprites[goodBugArray[goodBugNumber].bugName].events.onInputDown.add(function () {
                pubSubService.publish('game/bugZap/clicked', {
                    bug: goodBugArray[goodBugNumber],
                    sprite: bugSprites[goodBugArray[goodBugNumber].bugName]
                });
            }, this);

            //end forEach

            //Explains what each bug is, and asks them which is the bad bug. Randomizes it so the order isn't the same every time.
            if (firstBugsCount == false) {
                var coinFlip = Math.floor(Math.random() * 2);

                if (coinFlip == 1) {


                    window.setTimeout(function () {
                        voiceService.speakWithRobot("         Blow Up the Pest! Is the pest the " + goodBugArray[goodBugNumber].bugName + " or the " + pestArray[badBugNumber].bugName, 0.9, 0.9,
                            voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMRIGHT);
                    }, 3500);


                }
                else {

                    window.setTimeout(function () {
                        voiceService.speakWithRobot("       Blow up the Pest! Is the pest the " + pestArray[badBugNumber].bugName + " or the " + goodBugArray[goodBugNumber].bugName, 0.9, 0.9,
                            voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMRIGHT);
                    }, 3500);
                }
            }

        }

        function handleGameOver(){
            window.setTimeout(function(){
                //record all the student's responses
                if(currentTestService.status.bug_zap==false){
                    _.forEach(answers, function(answer){
                        currentTestService.recordAnswer(
                            answer.description, //description of the question
                            answer.studentChoice, //the student's choice
                            answer.correctChoice, //the correct choice
                            answer.chapter, // chapter in the JMG curriculum
                            answer.choices //array of strings listing all potential choices
                        );
                    });
                }

                //game is over!  Go back to lobby
                currentTestService.completeBugZap();
                navService.goToGameLobby();

            }, 750);
        }
        pubSubService.subscribe('game/bugZap/gameover', handleGameOver);

        function differentialDistance(dx, dy) {
            return Math.sqrt(dx*dx + dy*dy);
        }

        var currentState = $state.current.name;
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if(toState.name != currentState){
                    if(game){
                        game.destroy();
                        game = null;
                    }
                }
            });
    }]);