// This service keeps track of the current test session, including the selected assessment, student, and the answers given so far
app.service("currentTestService", [ '$log', 'localStorageService', 'testSessionService', 'navService',
    function($log, localStorage, testSessionService, navService){
        var DEBUG = false;
        var service = {
            currentStudent: null,
            currentAssessment: null,
            startTime: null,
            currentAnswers: [],
            status: {
                bug_zap: false,
                bug_sort: false,
                bug_match: false,
                plant_match: false,
                fruit_veggie: false
            }
        };

        service.reset = function(){
            service.currentAssessment = null;
            service.currentStudent = null;
            service.startTime = null;
            service.currentAnswers = [];
            service.status = {
                bug_zap: false,
                bug_sort: false,
                bug_match: false,
                plant_match: false,
                fruit_veggie: false
            };
            service.save();
        };

        service.completeBugZap = function(){
            service.status.bug_zap = true;
            service.save();
        };
        service.completeBugSort = function(){
            service.status.bug_sort = true;
            service.save();
        };
        service.completeBugMatch = function(){
            service.status.bug_match = true;
            service.save();
        };
        service.completePlantMatch = function(){
            service.status.plant_match = true;
            service.save();
        };
        service.completeFruitVeggie = function(){
            service.status.fruit_veggie = true;
            service.save();
        };

        service.recordAnswer = function(question, studentResponse, correctResponse, chapter, choices){
            //Builds the answer object and calls addAnswer()
            var choiceString = choices.join(', ');
            var answer = {
                question: question,
                studentChoice: studentResponse,
                correctChoice: correctResponse,
                chapter: chapter,
                choices: choiceString,
                timestamp: new Date()
            };

            service.addAnswer(answer);
        };

        service.addAnswer = function(answer){
            service.currentAnswers.push(answer);
            service.save();
        };

        service.startNewTest = function(student, assessment){
            service.reset();
            if (DEBUG) {
                $log.debug("Starting new test");
                //$log.debug(student);
                //$log.debug(assessment);
            }
            if(typeof student === 'object'){
                service.currentStudent = student;
            }
            else{
                service.currentStudent = JSON.parse(student);
            }
            if(typeof assessment === 'object'){
                service.currentAssessment = assessment;
            }
            else{
                service.currentAssessment = JSON.parse(assessment);
            }

            service.currentAnswers = [];
            service.startTime = new Date();
            service.save();
        };

        service.getCurrentAssessment = function(){
            service.load();
            return service.currentAssessment;
        };
        service.getCurrentStudent = function(){
            service.load();
            return service.currentStudent;
        };
        service.getCurrentAnswers = function(){
            service.load();
            return service.currentAnswers;
        };

        service.finishTest = function(){
            navService.showLoading("Please wait while results upload...");
            service.load();
            //Builds the test session object
            var testSession = {
                assessmentDate: new Date(),
                startTime: service.startTime,
                completionTime: new Date(),
                answers: service.currentAnswers,
                assessment: service.currentAssessment
            };
            var promise = testSessionService.create(testSession, service.currentStudent);
            var currentStudent = service.currentStudent;
            service.reset();

            return promise
                .then(function() {
                    navService.showFinished();
                })
                .catch(function(error){
                    var retryFunction = function(){
                        return testSessionService.create(testSession, currentStudent);
                    };
                    navService.showUploadError(retryFunction);
                });
        };

        service.save = function(){
            localStorage.set('currentTestStudent', service.currentStudent);
            localStorage.set('currentTestAssessment', service.currentAssessment);
            localStorage.set('currentTestAnswers', service.currentAnswers);
            localStorage.set('currentTestStartTime', service.startTime);
            localStorage.set('currentTestStatus', service.status);
        };

        service.load = function(){
            service.currentStudent = localStorage.get('currentTestStudent') || null;
            service.currentAssessment = localStorage.get('currentTestAssessment') || null;
            service.currentAnswers = localStorage.get('currentTestAnswers') || null;
            service.startTime = localStorage.get('currentTestStartTime') || null;
            service.status = localStorage.get('currentTestStatus') || {};
        };

        return service;
    }]);