/*
    This controller provides an example of a controller that drives a game using gameService and pubSubService.
    It has short examples of most of the things you can go with the two services.
    We may want to update this as we go to include examples of using the voiceService/robot and the missing methods from gameService
 */

//Declare a controller to be used for the game.  See the route-definition.js file to understand how these are wired up
//controllers take a list of a dependencies followed by a function.  The dependencies will be injected as arguments to the function
app.controller("gameExampleController", ['$scope', '$log', '$rootScope', '$state', 'navService', 'gameService', 'pubSubService', 'voiceService',
function($scope, $log, $rootScope, $state, navService, gameService, pubSubService, voiceService){
    $log.debug("Setting up game!");
    //The startNewGame method will create a new game instance that fills up the screen.
    var game = gameService.startNewGame(preload, create, update, 'game-container');

    //TODO: Make sure to include this each time we create a game.  Otherwise it will persist even if we navigate away from the page
    var currentState = $state.current.name;
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams){
            $log.debug("stateChangeStart!");
            if(toState.name != currentState){
                $log.debug("Destroying Game");
                game.destroy();
            }
        });

    var imageNames = {};
    var spriteSheetNames = {};
    var audioNames = {};

    var sprites = {};

    //The preload function is used to load assets like images, audio, and spritesheets
    function preload() {
        //most gameService methods require you to pass the game instance as the first argument.  see gameService.js for better docs
        //First we load a couple images and store the names by which we can reference them later
        imageNames.background = gameService.loadImage(game, 'bg', 'app/assets/images/backdrop.png');
        imageNames.rocket = gameService.loadImage(game, 'rocket', 'app/assets/images/rocket.png');
        //You can also load spritesheets
        spriteSheetNames.wasp = gameService.loadSpriteSheet(game, {name: "waspSheet", path: "app/assets/spritesheets/wasp_sprite.png", width: 100, height: 100})
    }

    //This method will be called after preload.  Here's where you can create sprites and wire up event listeners
    //Keep in mind that the gameService assumes a canvas size of 800x600.
    // Any coordinates or sizes should be specified relative to the size 800x600 and the service will scale for you.
    //For exmple, if you wanted an image to take up half the width and a third of the height, you would set its width to 400 and its height to 200
    function create() {
        //We'll create sprites from the images and sheets we loaded
        //First the background (takes up entire screen)
        sprites.bg = gameService.addSprite(game,
            0, 0, //x and y coordinates respectively
            800, 600, //width and height;  you can leave either one of these null and the image will retain its aspect ratio
            imageNames.background, // the name of the image to use.  The image MUST be preloaded in preload()
            false //optional indicator that will set the sprite's anchor to the center of the sprite.  By default this is false
        );
        //Now the rocket
        sprites.rocket = gameService.addSprite(game,
            200, 300,
            30, null, //I leave height null.  The rocket will be 30 pixels wide and the height will automatically scale
            imageNames.rocket,
            true //in this case, I do want the rocket's anchor to be its center
        );
        //And finally the wasp
        sprites.wasp = gameService.addSprite(game,
            650, 400,
            50, null,
            spriteSheetNames.wasp,
            true
        );

        // the wasp was made from a spritesheet, so it can be animated:
        //see Phaser docs for all the options for animation. This is a very simple exmaple that just loops through all the sprites in the spritesheet
        sprites.wasp.animations.add('move');
        sprites.wasp.play('move', 10, true);

        //I've added the ability for gameService to generate paths for sprites through a set of points.  Here's an example:
        //The current path generation algorithm will do a smooth interpolation that passes through all points
        var xCoords = [200, 400, 600, 400, 200];
        var yCoords = [300, 100, 300, 500, 300];
        var count = 150;  //how many points to interpolate;  This may change the speed of the sprite along the path;  more points = slower sprite
        sprites.rocket.path = gameService.generatePath(game, xCoords, yCoords, count);
        sprites.rocket.pathCounter = 0;  //Everything animated along a path will need some sort of counter like this.  Name it whatever you want

        //The create method should also wire up event dispatchers.  The pubSubService makes this easy:
        //cause the wasp to dispatch event when it's clicked;  Later on we'll create a method that responds to the event
        sprites.wasp.inputEnabled = true;
        sprites.wasp.events.onInputDown.add(function(){
            pubSubService.publish('game/example/clickedWasp',
                {tip: "Any event data you want to pass can be passed in this object", insult: "You are a stinky turd."});
        }, this);

        //Here's an example on how to use the voiceService/robot, although you should generally try NOT to use timers.
        window.setTimeout(function(){
            voiceService.speakWithRobot("Hello to all the pleebs.  You have been conquered by robotic overlords." +
                "Surrender or be annihilated!", 1.1, 1, voiceService.robots.INCORRECT, voiceService.robotLocations.BOTTOMLEFT)
                .then(function(){
                    alert("I get executed after the text has been spoken!");
                })
        }, 1500);

    }

    //The update function is called every frame.  If you've wired up event handlers properly, you shouldn't have to do much logic in here
    //One thing we use this for is animating sprites along paths
    function update() {
        //advance the rocket along its path
        advanceRocket()
    }

    //from here on out we're going to be defining functions that either handle events or make stuff happen in the update function
    var rocketMaxSpeed = 5; //pixels per frame.  We use this to avoid the rocket flying around sharp corners or behaving strangely.  You don't have to do this
    function advanceRocket(){
        var rocket = sprites.rocket;
        if(rocket.path){ //just a sanity check
            if(rocket.pathCounter>=rocket.path.length){
                rocket.pathCounter = 0;
            }
            var dx = rocket.path[rocket.pathCounter].x-rocket.x; //proposed change in the x direction
            var dy = rocket.path[rocket.pathCounter].y - rocket.y; //proposed change in the y direction
            var angle = rocket.path[rocket.pathCounter].angle; //the path also generates the angle so you can orient your sprite along its path if you choose
            var dist = distance(dx, dy);
            if(dist >= rocketMaxSpeed){ //if the rocket is going to move to fast, we scale its movement to obey our speed limit
                //scale dx and dy so that velocity is regulated
                var scaleFactor = rocketMaxSpeed/dist;
                dx = scaleFactor*dx;
                dy = scaleFactor*dy;
            } else{
                //We may only implement the path counter if maxVelocity wasn't exceeded.
                // If maxVelocity was exceeded, then we haven't reached the current path point yet, and we need another iteration to get there
                rocket.pathCounter++;
            }
            //update the rocket's position
            rocket.x +=dx;
            rocket.y +=dy;
            rocket.rotation = angle; //update the rocket's angle of inclination
        }
    }

    //Finally, we'll add an event listener for when our wasp is clicked.
    // This pattern of event handling allows us to remove a lot of messy logic from the preload, create, and update functions
    // We could even export all our event handling logic to a service if it gets too large
    var waspSubscription = pubSubService.subscribe('game/example/clickedWasp', waspClickHandler);
    function waspClickHandler(event){
        alert("The wasp says, \"" + event.insult + "\".");
    }

    // We can cancel the subscription at anytime by calling:
    //waspSubscription.unsubscribe();

    function distance(dx, dy){
        return Math.sqrt(dx*dx + dy*dy);
    }
}]);

