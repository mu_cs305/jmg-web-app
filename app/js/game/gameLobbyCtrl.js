app.controller("gameLobbyController", ['$scope', '$rootScope', 'currentTestService', 'navService', 'settings',
    function($scope,$rootScope, currentTestService, navService, settings){

        $scope.settings = settings;
        $scope.code = null;

        currentTestService.load();

        $scope.allDone = function(){
            var status = currentTestService.status;
            return status.bug_match && status.bug_sort && status.bug_zap && status.plant_match && status.fruit_veggie;
        };

        $scope.checkStatus = function(game){
            return currentTestService.status[game];
        };

        $scope.goToPlantMatch = function(){
            if($scope.allDone() || !$scope.checkStatus('plant_match')){
                navService.goToPlantMatch();
            }
        };
        $scope.goToBugMatch = function(){
            if($scope.allDone() || !$scope.checkStatus('bug_match')){
                navService.goToBugMatch();
            }
        };
        $scope.goToBugZap = function(){
            if($scope.allDone() || !$scope.checkStatus('bug_zap')){
                navService.goToBugZap();
            }
        };
        $scope.goToBugSort = function(){
            if($scope.allDone() || !$scope.checkStatus('bug_sort')){
                navService.goToBugSort();
            }
        };
        $scope.goToFruitVeggie = function(){
            if($scope.allDone() || !$scope.checkStatus('fruit_veggie')){
                navService.goToFruitVeggie();
            }
        };

        $scope.unlock = function(code){
            if($scope.settings.requirePin){
                if(code==settings.pin){
                    $scope.code = null;
                    Materialize.toast("Correct!", 2000);
                    $rootScope.toggleNav(true);
                } else{
                    Materialize.toast("Incorrect!", 2000);
                }
            } else{
                Materialize.toast("Correct!", 2000);
                $rootScope.toggleNav(true);
            }
        };

        $scope.lock = function(){
            $rootScope.toggleNav(false);
        };

        $scope.finish = function(){
            currentTestService.finishTest()
        };

    }]);