app.controller("uploadErrorController", ['$log', '$scope', '$stateParams', 'navService',
function($log, $scope, $stateParams, navService){
    $scope.loading = false;
    $scope.retry = function(){
        if($stateParams.retryFunction){
            $scope.loading = true;
            $stateParams.retryFunction()
                .then(function(){
                    navService.showFinished();
                })
                .finally(function(){
                    $scope.loading = false;
                });
        }
        else{
            Materialize.toast("Error!", 5000);
            navService.goToDashboard();
        }
    };

    $scope.abort = function(){
        navService.goToDashboard();
    };
}]);