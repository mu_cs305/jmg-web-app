﻿app.factory("accountService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, $q, CONFIG) {
    var service = {};

    service.apiUrl = CONFIG.API + 'api/';

    // Returns promise that will resolve with list of all user accounts
    // NOTE: ALL user account information is returned.  Make sure to hide sensitive fields
    service.getAll = function () {
        var deferred = $q.defer();
        $http.get(service.apiUrl + 'account/all')
            .then(function(response){
                deferred.resolve(response.data)
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getAdmins = function () {
        var deferred = $q.defer();
        $http.get(service.apiUrl + 'account/admins')
            .then(function(response){
                deferred.resolve(response.data)
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getInstructors = function () {
        var deferred = $q.defer();
        $http.get(service.apiUrl + 'account/instructors')
            .then(function(response){
                deferred.resolve(response.data)
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };

    //Returns a promise that will resolve with a list of all user accounts, even those that are marked deleted
    service.getAllUnfiltered = function () {
        var deferred = $q.defer();
        $http.get(service.apiUrl + 'account/all/unfiltered')
            .then(function(response){
                deferred.resolve(response.data)
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };

    //Returns a promise that will be resolved with the user object with the specified ID
    service.getById = function (id) {
        var deferred = $q.defer();

        $http.post(service.apiUrl + 'account/user', {
            id: id
        })
            .then(function(response){
                deferred.resolve(response.data)
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error)
            });

        return deferred.promise;
    };

    service.getByUsername = function (username) {
        var deferred = $q.defer();

        $http.post(service.apiUrl + 'account/self', {
            username: username
        })
            .then(function(response){
                deferred.resolve(response.data)
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error)
            });

        return deferred.promise;
    };

    // This method will attempt to create a new user account.
    // It should be passed a javascript object with all the required fields for a new user.
    // See commented out example at the end of the service defn
    service.createUser = function (registrationObj) {
        var deferred = $q.defer();
        $http.post(service.apiUrl + 'account/register', registrationObj)
            .then(function(response){
                deferred.resolve();  //You don't get anything back from this one.  Just a status code of 200
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };

    // This works just like createUser except you should pass an existing user object -
    // Just change the fields you want changed before calling this method
    // see example at the end of the service defn
    service.modifyUser = function (userObj) {

        var deferred = $q.defer();
        $http.put(service.apiUrl + 'account/user', userObj)
            .then(function(response){
                deferred.resolve(response.data);  //resolves with updated user object
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };

    // keep in mind that id is that ungodly long string that Microsoft generates, not the simple integer id we're all used to
    // something like aa239742-b43e-46fd-9c28-a4225d1b9eba
    service.deleteUser = function (id) {

        var deferred = $q.defer();
        $http({
            method: 'DELETE',
            url: service.apiUrl + 'account/user',
            data:{
                id: id
            },
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function(response){
                deferred.resolve(response.data); //resolves with deleted user object
            })
            .catch(function(error){
                $log.error("Error fetching accounts: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };


    service.changePassword = function(user, oldPassword, newPassword){
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: service.apiUrl + 'account/changepw',
            data:{
                id: user.id,
                oldPass: oldPassword,
                newPass: newPassword
            },
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function(response){
                deferred.resolve(response.data); //resolves with updated user object
            })
            .catch(function(error){
                $log.error("Error changing password: \n" + JSON.stringify(error));
                deferred.reject(error);
            });

        return deferred.promise;
    };

    // Not really sure if the userObj is supposed to be passed in here, or something else.
    service.resetPassword = function(userObj, newPassword, confirmNewPassword){
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: service.apiUrl + 'account/ResetPassword',
            data: {

            },
            params: {
                id: userObj.id,
                newPassword: newPassword,
                confirmNewPassword: confirmNewPassword,
            },
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function(response){
                deferred.resolve(response.data);
                $log("RESPONSE: " + response.data);
            })
            .catch(function(error){
                $log.error("Error performing password reset: " + JSON.stringify(error));
            });
        return deferred.promise;
    };

    return service;
}]);