// All this controller does is provide navigation functions since the href attribute isn't working for tab links

app.controller('accountTabsController', ['$rootScope', '$scope', '$state', 'userService', function($rootScope, $scope, $state, userService){

    $scope.isAdmin = userService.isAdmin();

    $scope.goToAdmins = function(){
        $state.go('app.account-management.admins');
    };
    $scope.goToInstructors = function(){
        $state.go('app.account-management.instructors');
    };
    $scope.goToStudents = function(){
        $state.go('app.account-management.students');
    };
}]);