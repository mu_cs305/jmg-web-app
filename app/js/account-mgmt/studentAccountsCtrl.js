app.controller("studentAccountsController", ['$scope', '$timeout', 'studentService',
    function($scope, $timeout, studentService){
        $scope.students = [];
        $scope.showArchived = "false";
        $scope.error = false;
        $scope.loading = false;

        $scope.options = {
            showArchived: false
        };

        // Stuff to control sorting table
        $scope.reverseOrder = false;
        $scope.orderOption = 'studentID';
        $scope.sort = function(item){
            if($scope.orderOption=='studentID'){
                return item.studentID;
            }
            else if($scope.orderOption=='dateCreated'){
                return new Date(item.registrationDate);
            }
            else return item.isDeleted;
        };
        $scope.chooseSorting = function(orderingField){
            if($scope.orderOption == orderingField){
                $scope.reverseOrder = !$scope.reverseOrder;
            }
            else{
                $scope.orderOption = orderingField;
                $scope.reverseOrder = false;
            }
        };

        //Stuff to control filtering:
        $scope.shouldFilter = function(student){
            return !student.isDeleted || $scope.options.showArchived;
        };

        $scope.data = {};
        $scope.data.newStudentID = null;

        var init = function(){
            $scope.loading = true;
            $scope.error = false;
            // get list of students to display:
            // Note: we get all students then apply the filter in the view (faster and sleeker in the long run)
            studentService.getStudentsUnfiltered()
                .then(function(students){
                    $scope.students = students;
                })
                .catch(function(error){
                    $scope.error = true;
                })
                .finally(function(){
                    $scope.loading = false;
                });
        };
        init();

        $scope.reload = function(){
            studentService.getStudentsUnfiltered()
                .then(function(students){
                    $scope.students = students;
                    $scope.error = false;
                })
                .catch(function(error){
                    $scope.error = true;
                });
        };

        $scope.addStudent = function(studentID){
            if($scope.idIsValid(studentID)){
                // Create student obj
                var student = {};
                student.studentID = studentID;
                student.registrationDate = new Date();
                student.isDeleted = false;
                student.testSessions = [];
                studentService.createStudent(student)
                    .then(function(){
                        // reload students:
                        $scope.reload();
                        Materialize.toast('Success!', 3000);
                        $scope.addError = null;
                    })
                    .catch(function(error){
                        //TODO: handle different error cases differently
                        Materialize.toast('Failed!', 4000);
                        if(error.status==409){
                            //Conflict error
                            $scope.addError = "A student with that ID already exists";
                        }
                        else{
                            $scope.addError = "Student could not be added at this time";
                        }
                    });
            }
        };

        $scope.acknowledge = function(){
            $scope.addError = null;
        };


        //Note this uses actual ID not studentID
        $scope.archiveStudent = function(student){
            studentService.deleteStudent(student)
                .then(function(){
                    Materialize.toast('Success!', 3000);
                })
                .catch(function(){
                    Materialize.toast('Failed!', 4000);
                })
                .finally($scope.reload);
        };

        $scope.unArchiveStudent = function(student){
            studentService.unarchiveStudent(student)
                .then(function(){
                    Materialize.toast('Success!', 3000);
                })
                .catch(function(){
                    Materialize.toast('Failed!', 4000);
                })
                .finally($scope.reload);
        };

        $scope.hasStudentsToShow = function(){
            var active = _.filter($scope.students, function(student){
                return student.isDeleted == false;
            });
            return (active.length>0) || ($scope.students.length>0 && $scope.options.showArchived);
        };

        $scope.idIsValid = function(id){
            var pattern = new RegExp("\[a-zA-Z]\[a-zA-Z]\[a-zA-Z]\[a-zA-Z]\\d\\d");
            return pattern.test(id);
        };

    }]);