app.factory("navService", ['$log', '$state', '$rootScope', 'pubSubService',
    function($log, $state, $rootScope, pubSubService){
    var service = {};

    service.goToLogin = function(){
        return $state.go("login");
    };

    service.goToDashboard = function(){
        return $state.go("app.dashboard");
    };

    service.goToAccountMgmt = function(){
        return $state.go("app.account-management");
    };

    service.goToStudentMgmt = function(){
        return $state.go("app.student-management");
    };

    service.goToTestMgmt = function(){
        return $state.go("app.test-management");
    };

    service.goToReports = function(){
        return $state.go("app.reports");
    };

    service.goToViewReport = function(report){
        return $state.go("app.view-report", {
            report: report
        });
    };

    service.goToStudentDetailReport = function(studentResult, report, studentID){
        $log.debug("Going to student detail report");
        return $state.go('app.student-detail-report', {
            result: studentResult,
            report: report,
            studentID: studentID
        });
    };

    service.goToTest = function(){
        return $state.go("app.test");
    };

    service.showLoading = function(message){
        return $state.go("app.loading", {message: message});
    };

    service.showFinished = function(){
        return $state.go("app.finished");
    };

    service.showUploadError = function(retryFunction){
        //retryFunction MUST return a promise
        return $state.go('app.upload-error', {retryFunction: retryFunction});
    };

    service.startGame = function(){
        //return $state.go('app.gameLobby', null, {reload: true});
        service.goToGameLobby();
    };
    service.goToBugMatch = function(){
        pubSubService.reset();
        //return $state.go("app.bug-zap", null, {reload: true});
        return $state.go("app.bug-match", null, {reload: true});
    };
    service.goToPlantMatch = function(){
        pubSubService.reset();
        return $state.go("app.plant-match", null, {reload:true});
    };
    service.goToBugZap = function(){
        pubSubService.reset();
        return $state.go("app.bug-zap", null, {reload:true});
    };
    service.goToBugSort = function(){
        pubSubService.reset();
        return $state.go("app.bug-sort", null, {reload:true});
    };
    service.goToFruitVeggie = function(){
        pubSubService.reset();
        return $state.go("app.fruit-veggie-game", null, {reload:true});
    };
    service.goToGameLobby = function(){
        return $state.go('app.gameLobby', null, {reload: true});
    };

    return service;
}]);