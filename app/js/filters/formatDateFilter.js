//Formats a date nicely
app.filter("formatDate", function(){
    return function(race_date){
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        formatted = "";
        var date = new Date(race_date);
        formatted += months[date.getMonth()] + ' ' + (date.getDate()+1) + ', ' + date.getFullYear();
        return formatted;

    };
});