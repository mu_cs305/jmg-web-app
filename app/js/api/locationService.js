app.factory("locationService", ['$http', '$q', 'CONFIG', function($http, $q, CONFIG){
    var service = {};

    service.getAll = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/locations";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getLocation = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/locations/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.updateLocation = function(id, data){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/locations/" + id;
        $http.put(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.createLocation = function(data){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/locations";
        $http.post(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);