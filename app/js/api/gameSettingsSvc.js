app.factory("gameSettingsService", ['$http', '$q', 'CONFIG', function($http, $q, CONFIG){
    var service = {};

    service.getAll = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/GameSettings";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.updateSettings = function(id, data){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/GameSettings/" + id;
        $http.put(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);