app.factory("imageService", ['$http', '$q', 'CONFIG', function($http, $q, CONFIG){
    var service = {};

    service.getAll = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/images";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getImage = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/images/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.updateImage = function(id, data){
        var deferred = $q.defer();

        var url = CONFIG.API + "api/images/" + id;
        $http.put(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.updateBugImage = function(bugID, imageID, data){
        var deferred = $q.defer();

        var url = CONFIG.API + "api/images/bug/" + bugID + "/" + imageID;
        $http.put(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.updatePlantImage = function(plantID, imageID, data){
        var deferred = $q.defer();

        var url = CONFIG.API + "api/images/plant/" + plantID + "/" + imageID;
        $http.put(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };


    service.createBugImage = function(imageData, bugID){
        var deferred = $q.defer();

        var url = CONFIG.API + "api/images/bug/" + bugID;
        $http.post(url, imageData)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
        return deferred.promise;
    };

    service.createPlantImage = function(imageData, plantID){
        var deferred = $q.defer();

        var url = CONFIG.API + "api/images/plant/" + plantID;
        $http.post(url, imageData)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
        return deferred.promise;
    };

    service.deleteImage = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "api/images/" + id;
        $http.delete(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
        return deferred.promise;
    };

    service.imageGetters = {
        //Gets the image to use for the bug match game
        getBugMatchImage: function (bug) {
            if (bug.images.length) {
                for (var i = 0; i < bug.images.length; i++) {
                    if (bug.images[i].bugMatchInd) {
                        return bug.images[i];
                    }
                }
                return bug.images[0]; //fallback if a bug-match picture wasn't found
            } else return null;
        },

        //Gets the image to use for the bug sort game
        getBugSortImage: function (bug) {
            if (bug.images.length) {
                for (var i = 0; i < bug.images.length; i++) {
                    if (bug.images[i].bugSortInd) {
                        //console.log("Returning early with: " + JSON.stringify(bug.images[i]));
                        return bug.images[i];
                    } else{
                        //console.log("Rejecting: " + JSON.stringify(bug.images[i]));
                    }
                }
                return bug.images[0]; //fallback if a bug-match picture wasn't found
            } else return null;
        },

        //Gets the image to use for the bug sort game
        getPlantMatchImage: function (plant) {
            if (plant.images.length) {
                for (var i = 0; i < plant.images.length; i++) {
                    if (plant.images[i].plantMatchInd) {
                        return plant.images[i];
                    }
                }
                return plant.images[0]; //fallback if a bug-match picture wasn't found
            } else return null;
        },

        //Gets the image to use for the fruit-veggie game
        getFruitVeggieImage: function (plant) {
            if (plant.images.length) {
                for (var i = 0; i < plant.images.length; i++) {
                    if (plant.images[i].fruitVeggieInd) {
                        return plant.images[i];
                    }
                }
                return plant.images[0]; //fallback if a bug-match picture wasn't found
            } else return null;
        }
    };

    return service;

}]);