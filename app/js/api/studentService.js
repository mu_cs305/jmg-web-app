app.factory("studentService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, $q, CONFIG){
    var service = {};

    //TODO: test these once we actually have students

    service.getStudents = function(){
        //TODO: test
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/students";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getStudentsUnfiltered = function(){
        //TODO: test
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/students/unfiltered";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getStudent = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/students/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.createStudent = function(student){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/students";
        $http.post(url, student)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.deleteStudent = function(student){
        //TODO: test
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/students/archive";
        $http.post(url, student)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.unarchiveStudent = function(student){
        //TODO: test
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/students/unarchive";
        $http.post(url, student)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);