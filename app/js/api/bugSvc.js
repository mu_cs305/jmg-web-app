app.factory("bugService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, $q, CONFIG){
    var service = {};

    service.getAll = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/bugs";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getBugTypes = function(){
        return {
            0: "Predator",
            1: "Pollinator",
            2: "Pooper",
            3: "Pest"
        }
    };

    service.getBug = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/bugs/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.updateBug = function(id, data){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/bugs/" + id;
        $http.put(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getBugLists = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/buglists";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getBugList = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/buglists/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);