app.controller('locationMgmtController', ['$scope', 'locationService', function($scope, locationService){
    $scope.loading = true;
    $scope.error = null;
    $scope.clearError = function(){$scope.error = null};

    $scope.newLocation = {
        name: null,
        active: true
    };

    $scope.locations = [];

    // Stuff to control sorting table
    $scope.reverseOrder = false;
    $scope.orderOption = 'name';
    $scope.sort = function(item){
        if($scope.orderOption=='name'){
            return item.name;
        }
        else return item.active;
    };
    $scope.chooseSorting = function(orderingField){
        if($scope.orderOption == orderingField){
            $scope.reverseOrder = !$scope.reverseOrder;
        }
        else{
            $scope.orderOption = orderingField;
            $scope.reverseOrder = false;
        }
    };


    $scope.load = function(){
        locationService.getAll()
            .then(function(locations){
                $scope.locations = locations;
            }, function(error){
                $scope.error = "Uh oh!  Something went wrong retrieving locations. Please try again later.";
            })
            .finally(function(){
                $scope.loading = false;
            });
    };
    $scope.load();

    $scope.addLocation = function(){
        $scope.loading = true;
        locationService.createLocation($scope.newLocation)
            .then(function(location){
                $scope.locations.push(location);
                $scope.newLocation = {
                    name: null,
                    active: true
                }
            }, function(error){
                Materialize.toast("Error!", 3000);
            })
            .finally(function(){
                $scope.loading = false;
            })
    };

    $scope.updateLocation = function(location){
        locationService.updateLocation(location.locationID, location)
            .then(function(updated){
                location.name = updated.name;
                location.active = updated.active;
                Materialize.toast("Updated!", 2500);
            }, function(error){
                location.active = !location.active;
                Materialize.toast("Error!", 3000);
            })
    };
}]);