﻿app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'app/templates/login/login.html',
            controller: 'loginController',
            params: {
                specialMessage: null
            }
        })
        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'app/templates/menu.html',
            controller: 'menuController'
        })
        .state('app.dashboard', {
            url: '/dashboard',
            views: {
                'app': {
                    templateUrl: 'app/templates/dashboard/dashboard.html',
                    controller: 'dashboardController'
                }
            }
        })
        .state('app.account-management', {
            url: '/accounts',
            abstract: true,
            //templateUrl: "app/templates/account-mgmt/account-mgmt-tabs.html"
            views: {
                'app': {
                    abstract: true,
                    templateUrl: 'app/templates/account-mgmt/account-mgmt-tabs.html',
                    controller: "accountTabsController"
                }
            }
        })
        .state('app.account-management.admins', {
            url: '/admins',
            views: {
                'adminTab': {
                    templateUrl: "app/templates/account-mgmt/admin-mgmt.html",
                    controller: "adminAccountsController"
                }
            }
        })
        .state('app.account-management.instructors', {
            url: '/instructors',
            views: {
                'instructorTab': {
                    templateUrl: "app/templates/account-mgmt/instructor-mgmt.html",
                    controller: "instructorAccountsController"
                }
            }
        })
        .state('app.account-management.students', {
            url: '/students',
            views: {
                'studentTab': {
                    templateUrl: "app/templates/account-mgmt/student-mgmt.html",
                    controller: "studentAccountsController"
                }
            }
        })
        .state('app.my-account',{
            url: '/my-account',
            views: {
                'app': {
                    templateUrl: 'app/templates/account-mgmt/my-account.html',
                    controller: 'myAccountController'
                }
            }
        })
        .state('app.student-management', {
            url: '/students',
            views: {
                'app': {
                    templateUrl: 'app/templates/student-mgmt/student-mgmt.html',
                    controller: "studentController"
                }
            }
        })
        .state('app.test-management', {
            url: '/test',
            views: {
                'app': {
                    templateUrl: 'app/templates/test-mgmt/test-mgmt.html'
                }
            }
        })
        .state('app.reports', {
            url: '/reports',
            views: {
                'app': {
                    templateUrl: 'app/templates/reports/reports.html',
                    controller: "reportsController"
                }
            }
        })
        .state('app.view-report', {
            url: '/view-report',
            views: {
                'app': {
                    templateUrl: 'app/templates/reports/view-report.html',
                    controller: "viewReportsController"
                }
            },
            params: {
                report: null
            }
        })
        .state('app.student-detail-report', {
            url: '/student-detail-report',
            views: {
                'app': {
                    templateUrl: 'app/templates/reports/student-detail-report.html',
                    controller: "studentDetailReportsController"
                }
            },
            params: {
                result: null,
                report: null,
                studentID: null
            }
        })
        .state('app.test-start', {
            url: '/start',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/test-start.html',
                    controller: "testStartController"
                }
            }
        })
        .state('app.gameLobby', {
            url: '/lobby',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-lobby.html',
                    controller: "gameLobbyController",
                    resolve: {
                        'settings': function(gameSettingsService){
                            return gameSettingsService.getAll()
                                .then(function(data){
                                    return data[0];
                                });
                        }
                    }
                }
            }
        })
        .state('app.seed-packets', {
            url: '/seed-packets',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-container.html',
                    controller: "seedPacketGameController"
                }
            }
        })
        .state('app.bug-zap', {
            url: '/bug-zap',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-container.html',
                    controller: "bugZapGameController",
                    resolve: {
                        'settings': function(gameSettingsService){
                            return gameSettingsService.getAll()
                                .then(function(data){
                                    return data[0];
                                });
                        }
                    }
                }
            }
        })
        .state('app.bug-sort', {
            url: '/bug-sort',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-container.html',
                    controller: "bugSortGameController",
                    resolve: {
                        'settings': function(gameSettingsService){
                            return gameSettingsService.getAll()
                                .then(function(data){
                                    return data[0];
                                });
                        },
                        allBugs: function($log, bugService){
                            return bugService.getAll()
                                .then(function(bugs){
                                    return bugs;
                                });
                        }
                    }
                }
            }
        })
        .state('app.plant-parts', {
            url: '/plant-parts',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-container.html',
                    controller: "plantPartsGameController"
                }
            }
        })
        .state('app.edible-parts', {
            url: '/edible-parts',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-container.html',
                    controller: "ediblePartsGameController"
                }
            }
        })
        .state('app.bug-match', {
            url: '/bug-match',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/bug-match.html',
                    controller: 'bugMatchGameController'
                }
            },
            resolve:{
                allBugs: function($log, bugService){
                    return bugService.getAll()
                        .then(function(bugs){
                            return bugs;
                        });
                },
                'settings': function(gameSettingsService){
                    return gameSettingsService.getAll()
                        .then(function(data){
                            return data[0];
                        });
                }
            }
        })
        .state('app.plant-match', {
            url: '/plant-match',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/plant-match.html',
                    controller: 'plantMatchGameController'
                }
            },
            resolve:{
                allPlants: function($log, plantService){
                    return plantService.getPlants()
                        .then(function(plants){
                            return plants;
                        });
                },
                'settings': function(gameSettingsService){
                    return gameSettingsService.getAll()
                        .then(function(data){
                            return data[0];
                        });
                }
            }
        })
        .state('app.fruit-veggie-game', {
            url: '/fruit-veggie',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-container.html',
                    controller: 'fruitVeggieGameController'
                }
            },
            resolve:{
                allPlants: function($log, plantService){
                    return plantService.getPlants()
                        .then(function(plants){
                            return plants;
                        });
                },
                'settings': function(gameSettingsService){
                    return gameSettingsService.getAll()
                        .then(function(data){
                            return data[0];
                        });
                }
            }
        })
        .state('app.game-example', {
            url: '/example',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/game-container.html',
                    controller: 'gameExampleController'
                }
            }
        })
        .state('app.loading', {
            url: '/loading',
            views: {
                'app': {
                    templateUrl: 'app/templates/loading.html',
                    controller: 'loadingController'
                }
            },
            params: {
                message: null
            }
        })
        .state('app.finished', {
            url: '/finished',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/finished.html'
                }
            }
        })
        .state('app.upload-error', {
            url: '/upload-error',
            views: {
                'app': {
                    templateUrl: 'app/templates/game/upload-error.html',
                    controller: "uploadErrorController"
                }
            },
            params: {
                retryFunction: null  //retryFunction MUST return a promise
            }
        })
        .state('app.denied', {
            url: '/denied',
            views: {
                'app': {
                    templateUrl: 'app/templates/denied.html'
                }
            }
        });

    //For some reason, we get an infinite loop if we set a default url
    //$urlRouterProvider.otherwise('/login');
}]);
