﻿//TODO: we can also add a check for an active access token here if we decide we need to.

app.service("routeChangeInterceptor", ['$log', '$state', 'CONFIG', '$rootScope', 'userService',
    function($log, $state, CONFIG, $rootScope, userService){
        var service = {};

        //Add states here to require admin access
        service.adminStates = [
            "app.account-management",
            "app.account-management.instructors",
            "app.account-management.admins",
            "app.student-management",
            "app.test-management"
        ];

        service.gameStates = [
            "app.gameLobby",
            "app.bug-match",
            "app.plant-match",
            "app.bug-zap",
            "app.bug-sort",
            "app.fruit-veggie-game"
        ];

        //Call this function to start intercepting state changes
        service.listen = function(){
            //Handler for user-related issues (non-admin, not-logged-in, etc.)
            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    var user = userService.getUser();
                    if(!user && toState.name!='login') {
                        //if we don't have a valid user, redirect to login
                        event.preventDefault();
                        $state.go('login', {
                            specialMessage: "Login has expired.  Please login again."
                        });
                    }
                    else if(user && !user.isAdmin){
                        //check if user is trying to access an admin-only state
                        if(_.includes(service.adminStates, toState.name)){
                            $log.warn("Non-admin prevented from accessing state: " + toState.name);
                            //prevent state change
                            event.preventDefault();
                            //redirect to denied page:
                            $state.go("app.denied");
                        }
                    }

                });

            //Handler for navigating to a game page that shouldn't have a navbar
            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    if(_.includes(service.gameStates, toState.name)){
                        $rootScope.toggleNav(false);
                    } else{
                        $rootScope.toggleNav(true);
                    }
                });

            //Handler for navigating from the game lobby to another page
            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    if(fromState.name == "app.gameLobby"){
                        if(!_.includes(service.gameStates, toState.name) && toState.name!="app.loading" && toState.name!="app.finished"){
                            if(!confirm("Are you sure you want to navigate away from this page?  The student's results will not be saved.")){
                                event.preventDefault();
                            }
                        }
                    }

                });
        };

        return service;
    }]);
